jQuery(document).ready(function(){
      fixHeight('.services-intro > div');
      fixHeight('.services-assist > div');
      fixHeight('.services-prevention > div');
      fixHeight('.services-consult > div');
      fixHeight('.services-salud > div');
      fixHeight('.services-forma > div');
});
function fixHeight(elements){
  var max = 0;
  jQuery(''+elements+'').each(function(){
     var h = jQuery(this).height();
     if(h>max){
        max = h;
     }
   });
   jQuery(''+elements+'').css("min-height", max+20); 
}