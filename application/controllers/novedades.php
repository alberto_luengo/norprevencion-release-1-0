<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Novedades extends MY_Controller {

	public function __construct(){
		parent::__construct();
	}

	public function index(){

		$key = strtolower(__CLASS__);

		$this->setMetas($key);
		$this->menu($key);

		$this->load->view('sections/'. $key , $this->data);

	}

}
