<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contacto extends MY_Controller {

	public function __construct(){
		parent::__construct();
	}

	public function index(){

		$this->data['msn'] = FALSE;

		$key = strtolower(__CLASS__);

		$this->setMetas($key);
		$this->menu($key);

		if($_POST){

			$this->config->load('email');

			$this->load->library('email');
			$config['charset'] = 'utf-8';
			$config['useragent'] = 'norprevencion';
			$config['mailtype'] = 'html';
			$this->email->initialize($config);

			$this->email->from($this->config->item('email_from'), $this->config->item('email_name'));
			$this->email->to($this->config->item('email_to_contacto'));

			$this->email->subject('Formulario de contacto web');

			$this->data['datos'] = $_POST;
			$tpl_mail = $this->load->view('mails/'. $key , $this->data , TRUE);

			$this->email->message($tpl_mail);

			if($this->email->send()){
				$this->data['msn'] = 'Correo enviado correctamente. Gracias.';
			}else{
				$this->data['msn'] = 'Error el enviar el correo, inténtelo en unos instantes. Gracias.';
			}


		}


		$this->load->view('sections/'. $key , $this->data);

	}

}
