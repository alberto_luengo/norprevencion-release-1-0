<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class legal extends MY_Controller {

        private $_section;

        public function __construct(){
                parent::__construct();
                $this->_section = strtolower(__CLASS__);
                $this->data['SHOW_SUBMENU'] = FALSE;
        }

        public function index(){
            show_404();
        }


        public function politica(){
                $this->_loadSubSection(strtolower(__FUNCTION__));
        }

        public function contenido(){
                $this->_loadSubSection(strtolower(__FUNCTION__));
        }

        public function responsabilidades(){
                $this->_loadSubSection(strtolower(__FUNCTION__));
        }


        /**
         * Load sub sections on services
         * @param  string $key subsection function name
         */
        private function _loadSubSection($key){


                $this->setMetas($key);
                $this->menu($this->_section);
                $this->submenu($key);

                $this->load->view('sections/'. $this->_section .'/'. $key , $this->data);

        }



}