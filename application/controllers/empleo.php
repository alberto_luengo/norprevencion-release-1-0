<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Empleo extends MY_Controller {

	public function __construct(){
		parent::__construct();
	}

	public function index(){

		$this->data['msn'] = FALSE;
		$has_file = FALSE;

		$key = strtolower(__CLASS__);

		$this->setMetas($key);
		$this->menu($key);

		if($_POST){

			if(isset($_FILES['userfile']['tmp_name']) && trim($_FILES['userfile']['tmp_name']!='') ){

				$file_tmp = $_FILES['userfile']['tmp_name'];

				$config['upload_path'] = './data/cv/';
				$config['allowed_types'] = '*';
				$config['max_size']	= '10000'; // Kb - 10Mb

				$this->load->library('upload', $config);

				if ( ! $this->upload->do_upload()){
					$this->data['msn'] = 'Error al subir el CV. Compruebe su tamaño e inténtelo de nuevo. Gracias.';
					$this->load->view('sections/'. $key , $this->data);
					return;
				}else{
					$data = $this->upload->data();
					$file_path = $data['file_path'].$data['file_name'];
				}

				$has_file = TRUE;
			}

			$this->config->load('email');

			$this->load->library('email');
			$config['charset'] = 'utf-8';
			$config['useragent'] = 'norprevencion';
			$config['mailtype'] = 'html';
			$this->email->initialize($config);

			$this->email->from($this->config->item('email_from'), $this->config->item('email_name'));
			$this->email->to($this->config->item('email_to_empleo'));

			if($has_file){
				$this->email->attach($file_path);
			}

			$this->email->subject('Formulario de empleo web');

			$this->data['datos'] = $_POST;
			$tpl_mail = $this->load->view('mails/'. $key , $this->data , TRUE);

			$this->email->message($tpl_mail);

			if($this->email->send()){
				$this->data['msn'] = 'Correo enviado correctamente. Gracias.';
			}else{
				$this->data['msn'] = 'Error el enviar el correo, inténtelo en unos instantes. Gracias.';
			}

			@unlink($file_path);

		}

		$this->load->view('sections/'. $key , $this->data);

	}

}
