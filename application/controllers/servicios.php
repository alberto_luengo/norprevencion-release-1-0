<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Servicios extends MY_Controller {

        private $_section;

        public function __construct(){
                parent::__construct();
                $this->_section = strtolower(__CLASS__);
                $this->data['SHOW_SUBMENU'] = TRUE;
        }

        public function index(){

        $this->setMetas($this->_section);
                $this->menu($this->_section);
                $this->submenu(false);

                $this->load->view('sections/'. $this->_section , $this->data);

        }


        public function ajenos(){
                $this->_loadSubSection(strtolower(__FUNCTION__));
        }

        public function asistencia(){
                $this->_loadSubSection(strtolower(__FUNCTION__));
        }

        public function consultoria(){
                $this->_loadSubSection(strtolower(__FUNCTION__));
        }

        public function vigilancia(){
                $this->_loadSubSection(strtolower(__FUNCTION__));
        }
        
        public function formacion(){
                $this->_loadSubSection(strtolower(__FUNCTION__));
        }

        /**
         * Load sub sections on services
         * @param  string $key subsection function name
         */
        private function _loadSubSection($key){


                $this->setMetas($key);
                $this->menu($this->_section);
                $this->submenu($key);

                $this->load->view('sections/'. $this->_section .'/'. $key , $this->data);

        }



}