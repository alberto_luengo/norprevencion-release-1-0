<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MY_Controller {

	public function __construct(){
		parent::__construct();
	}

	public function index(){

		$this->data['PROMO'] = TRUE;
		$this->menu('home');
		$this->load->view('sections/home' , $this->data);

	}

}

