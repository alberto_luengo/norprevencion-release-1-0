<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Css extends MY_Controller {

	public function __construct(){
		parent::__construct();
	}

	public function index(){

		$this->load->library('lessc');

		$css = array();
		$css[] = "css/reticulas/core/reset.css";
		$css[] = "css/reticulas/core/base.css";
		$css[] = "css/reticulas/core/form.css";
		$css[] = "css/reticulas/core/utilities.less";
		$css[] = "css/reticulas/grids/fluid.less";
		$css[] = "css/extended/norprevencion.less";

		$file = '';
        foreach ($css as $item) {
            $file .= file_get_contents($item);
        }
        header('Content-type: text/css');
        $less = new lessc();
        $less->setFormatter("compressed");
        echo $css = $less->parse($file);
        exit;

	}

}

