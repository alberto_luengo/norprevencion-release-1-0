<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Presupuestos extends MY_Controller {

	public function __construct(){
		parent::__construct();
	}

	public function index(){

		$this->data['msn'] = FALSE;

		$key = strtolower(__CLASS__);

		$this->setMetas($key);
		$this->menu($key);

		if($_POST){

			$this->data['name'] = trim($_POST['name']);
		    $this->data['mail'] = trim($_POST['mail']);
		    $this->data['comment'] = trim($_POST['comment']);
			$this->data['centers'] = array();

			foreach ($_POST as $index => $value) {
				$explode = explode("_",$index);
				if(count($explode)==2){
					$this->data['centers'][$explode[1]][$explode[0]] = $value;
				}
			}

			$this->config->load('email');

			$this->load->library('email');
			$config['charset'] = 'utf-8';
			$config['useragent'] = 'norprevencion';
			$config['mailtype'] = 'html';
			$this->email->initialize($config);

			$this->email->from($this->config->item('email_from'), $this->config->item('email_name'));
			$this->email->to($this->config->item('email_to_presupuestos'));

			$this->email->subject('Formulario de presupuesto web');

			$tpl_mail = $this->load->view('mails/'. $key , $this->data , TRUE);

			$this->email->message($tpl_mail);

			if($this->email->send()){
				$this->data['msn'] = 'Correo enviado correctamente. Gracias.';
			}else{
				$this->data['msn'] = 'Error el enviar el correo, inténtelo en unos instantes. Gracias.';
			}


		}


		$this->load->view('sections/'. $key , $this->data);

	}

	public function addcenter(){
		$this->data['DELETE_FORM'] = TRUE;
		$this->load->view('sections/presupuestos_centers_form', $this->data);
	}

}
