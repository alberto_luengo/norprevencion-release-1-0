<?php

/**
 * Cuenta desde la que llega el correo, debe estar creada
 * en el servidor para pasar uno de los filtros de spam
 */
$config['email_from'] = 'info@norprevencion.com';

/**
 * Nombre from
 */
$config['email_from_name'] = 'Norprevención';

/**
 * Presupuestos
 */
$config['email_to_presupuestos'] = 'comercial@norprevencion.com';

/**
 * Trabaja con nosotros
 */
$config['email_to_empleo'] = 'rrhh@norprevencion.com';

/**
 * Contacto
 */
$config['email_to_contacto'] = 'info@norprevencion.com';

?>