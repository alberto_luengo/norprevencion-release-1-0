<?php (defined('BASEPATH')) OR exit('No direct script access allowed');


class MY_Controller extends MX_Controller {


	protected $data;

	public function __construct(){

		parent::__construct();

		$this->data = array();

        $this->load->language('global');

        $this->data['PROMO'] = FALSE;
        $this->data['SHOW_SUBMENU'] = FALSE;

        $this->load->helper('url');
        $url = str_replace(base_url(), '', current_url());
        $url = str_replace(LANG.'/'.LANG, '', $url );
        define('URL', $url);

        /**
         * Set default metas
         */
        $this->metas( $this->lang->line('global_meta_title'),
                      $this->lang->line('global_meta_description'),
                      $this->lang->line('global_meta_keywords')
        );

	}

    /**
     * Set metas helper function
     * @param string $key string id
     */
    protected function setMetas($key){

        $this->metas( $this->lang->line($key . '_meta_title'),
                      $this->lang->line($key . '_meta_description'),
                      $this->lang->line($key . '_meta_keywords')
        );

    }


    /**
     * Metas generation
     * @param string $title
     * @param string $description
     * @param string $keywords
     */
    protected function metas($title,
                             $description,
                             $keywords
                             ){

        $this->data['meta_title'] = $title;
        $this->data['meta_description'] = $description;
        $this->data['meta_keywords'] = $keywords;
    }

    /**
     * Menu helper
     * @param  string $menu menu
     * @return string       menu
     */
    protected function menu($menu){
      return $this->data['menu'] = $menu;
    }

    /**
     * Submenu helper
     * @param  string $submenu submenu
     * @return string          submenu
     */
    protected function submenu($submenu){
      return $this->data['submenu'] = $submenu;
    }


	public function __destruct(){
	}

}