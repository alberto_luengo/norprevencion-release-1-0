<?php


// ==================================================================
//
// Metas Apartados generales
//
// ------------------------------------------------------------------


$lang['global_meta_title'] = 'Norprevención – Servicio de Prevención de Riesgos Laborales';
$lang['global_meta_description'] = 'Norprevención, prevención de riesgos laborales, higiene industrial, seguridad, medicina del trabajo, formación, consultoría y psicosociología en Lugo';
$lang['global_meta_keywords'] = 'norprevencion, lugo, prevención, riesgos, laborales, higiene industrial, psicosociología, medicina del trabajo, psicosociología, formación';


$lang['servicios_meta_title'] = 'Servicios - Norprevención – Servicio de Prevención de Riesgos Laborales';
$lang['servicios_meta_description'] = 'Servicios Norprevención, prevención de riesgos laborales, higiene industrial, seguridad, medicina del trabajo, formación, consultoría y psicosociología en Lugo';
$lang['servicios_meta_keywords'] = 'servicios, norprevencion, lugo, prevención, riesgos, laborales, higiene industrial, psicosociología, medicina del trabajo, psicosociología, formación';

$lang['centros_meta_title'] = 'Red de centros - Norprevención – Servicio de Prevención de Riesgos Laborales';
$lang['centros_meta_description'] = 'Red de centros Norprevención, prevención de riesgos laborales, higiene industrial, seguridad, medicina del trabajo, formación, consultoría y psicosociología en Lugo';
$lang['centros_meta_keywords'] = 'centros, red, norprevencion, lugo, prevención, riesgos, laborales, higiene industrial, psicosociología, medicina del trabajo, psicosociología, formación';


$lang['novedades_meta_title'] = 'Novedades - Norprevención – Servicio de Prevención de Riesgos Laborales';
$lang['novedades_meta_description'] = 'Novedades Norprevención, prevención de riesgos laborales, higiene industrial, seguridad, medicina del trabajo, formación, consultoría y psicosociología en Lugo';
$lang['novedades_meta_keywords'] = 'novedades, norprevencion, lugo, prevención, riesgos, laborales, higiene industrial, psicosociología, medicina del trabajo, psicosociología, formación';


$lang['contacto_meta_title'] = 'Contacto - Norprevención – Servicio de Prevención de Riesgos Laborales';
$lang['contacto_meta_description'] = 'Contacto Norprevención, prevención de riesgos laborales, higiene industrial, seguridad, medicina del trabajo, formación, consultoría y psicosociología en Lugo';
$lang['contacto_meta_keywords'] = 'contacto, norprevencion, lugo, prevención, riesgos, laborales, higiene industrial, psicosociología, medicina del trabajo, psicosociología, formación';


$lang['empleo_meta_title'] = 'Trabaja con nosotros - Norprevención – Servicio de Prevención de Riesgos Laborales';
$lang['empleo_meta_description'] = 'Trabaja con nosotros Norprevención, prevención de riesgos laborales, higiene industrial, seguridad, medicina del trabajo, formación, consultoría y psicosociología en Lugo';
$lang['empleo_meta_keywords'] = 'empleo, trabajo, servicios, norprevencion, lugo, prevención, riesgos, laborales, higiene industrial, psicosociología, medicina del trabajo, psicosociología, formación';


$lang['presupuestos_meta_title'] = 'Presupuestos - Norprevención – Servicio de Prevención de Riesgos Laborales';
$lang['presupuestos_meta_description'] = 'Presupuestos Norprevención, prevención de riesgos laborales, higiene industrial, seguridad, medicina del trabajo, formación, consultoría y psicosociología en Lugo';
$lang['presupuestos_meta_keywords'] = 'presupuestos, servicios, norprevencion, lugo, prevención, riesgos, laborales, higiene industrial, psicosociología, medicina del trabajo, psicosociología, formación';



// ==================================================================
//
// Metas Servicios  - Subapartados
//
// ------------------------------------------------------------------



$lang['ajenos_meta_title'] = 'Ajenos de prevención - Norprevención – Servicio de Prevención de Riesgos Laborales';
$lang['ajenos_meta_description'] = 'Ajenos de prevención Norprevención, prevención de riesgos laborales, higiene industrial, seguridad, medicina del trabajo, formación, consultoría y psicosociología en Lugo';
$lang['ajenos_meta_keywords'] = 'ajenos, prevención, servicios, norprevencion, lugo, prevención, riesgos, laborales, higiene industrial, psicosociología, medicina del trabajo, psicosociología, formación';

$lang['asistencia_meta_title'] = 'Asistencia Técnica - Norprevención – Servicio de Prevención de Riesgos Laborales';
$lang['asistencia_meta_description'] = 'Asistencia Técnica Norprevención, prevención de riesgos laborales, higiene industrial, seguridad, medicina del trabajo, formación, consultoría y psicosociología en Lugo';
$lang['asistencia_meta_keywords'] = 'asistencia, tecnica, servicios, norprevencion, lugo, prevención, riesgos, laborales, higiene industrial, psicosociología, medicina del trabajo, psicosociología, formación';

$lang['consultoria_meta_title'] = 'Consultoría y Servicios Especiales - Norprevención – Servicio de Prevención de Riesgos Laborales';
$lang['consultoria_meta_description'] = 'Consultoría y Servicios Especiales Norprevención, prevención de riesgos laborales, higiene industrial, seguridad, medicina del trabajo, formación, consultoría y psicosociología en Lugo';
$lang['consultoria_meta_keywords'] = 'consultoria, especiales, servicios, norprevencion, lugo, prevención, riesgos, laborales, higiene industrial, psicosociología, medicina del trabajo, psicosociología, formación';

$lang['vigilancia_meta_title'] = 'Vigilancia de la Salud - Norprevención – Servicio de Prevención de Riesgos Laborales';
$lang['vigilancia_meta_description'] = 'Vigilancia de la Salud Norprevención, prevención de riesgos laborales, higiene industrial, seguridad, medicina del trabajo, formación, consultoría y psicosociología en Lugo';
$lang['vigilancia_meta_keywords'] = 'vigilancia, salud, servicios, norprevencion, lugo, prevención, riesgos, laborales, higiene industrial, psicosociología, medicina del trabajo, psicosociología, formación';


$lang['formacion_meta_title'] = 'Formación - Norprevención – Servicio de Prevención de Riesgos Laborales';
$lang['formacion_meta_description'] = 'Formación Norprevención, prevención de riesgos laborales, higiene industrial, seguridad, medicina del trabajo, formación, consultoría y psicosociología en Lugo';
$lang['formacion_meta_keywords'] = 'formacion, servicios, norprevencion, lugo, prevención, riesgos, laborales, higiene industrial, psicosociología, medicina del trabajo, psicosociología, formación';


// ==================================================================
//
// Metas Legal
//
// ------------------------------------------------------------------


$lang['politica_meta_title'] = 'Política de protección de datos - Norprevención – Servicio de Prevención de Riesgos Laborales';
$lang['politica_meta_description'] = 'Política de protección de datos Norprevención, prevención de riesgos laborales, higiene industrial, seguridad, medicina del trabajo, formación, consultoría y psicosociología en Lugo';
$lang['politica_meta_keywords'] = 'política, protección, datos, servicios, norprevencion, lugo, prevención, riesgos, laborales, higiene industrial, psicosociología, medicina del trabajo, psicosociología, formación';

$lang['responsabilidades_meta_title'] = 'Responsabilidades y resolución de conflictos - Norprevención – Servicio de Prevención de Riesgos Laborales';
$lang['responsabilidades_meta_description'] = 'Responsabilidades y resolución de conflictos Norprevención, prevención de riesgos laborales, higiene industrial, seguridad, medicina del trabajo, formación, consultoría y psicosociología en Lugo';
$lang['responsabilidades_meta_keywords'] = 'responsabilidades, resolución, conflictos, servicios, norprevencion, lugo, prevención, riesgos, laborales, higiene industrial, psicosociología, medicina del trabajo, psicosociología, formación';

$lang['contenido_meta_title'] = 'Contenido y uso de la web - Norprevención – Servicio de Prevención de Riesgos Laborales';
$lang['contenido_meta_description'] = 'Contenido y uso de la web Norprevención, prevención de riesgos laborales, higiene industrial, seguridad, medicina del trabajo, formación, consultoría y psicosociología en Lugo';
$lang['contenido_meta_keywords'] = 'contenido, uso, formacion, servicios, norprevencion, lugo, prevención, riesgos, laborales, higiene industrial, psicosociología, medicina del trabajo, psicosociología, formación';


// ==================================================================
//
// Home section
//
// ------------------------------------------------------------------

$lang['about_us_header'] = 'About Us: A Brief History of Norprevención';
$lang['about_us_p1'] = '<b>Norprevención, SL External Prevention Service</b>, born as a Society accredited Occupational Risk Prevention on June 2, 1999, with, at first, with a Central Office in Lugo and a second delegation in Vigo (Pontevedra) by which were two technicians and two administrative.';
$lang['about_us_p2'] = 'From that time until the present Norprevención has experienced tremendous growth, and at present with around two hundred professionals distributed Delegations by the autonomous communities of Galicia, Castilla y Leon, Madrid, Asturias and Catalonia, placing as first External Prevention Service of Galicia in number of companies and the first in this region to get the final accreditation to practice preventive activity throughout the national territory.';
$lang['about_us_p3'] = 'Throughout these years, we have collaborated on many projects in prevention:';
$lang['about_us_list'] = '
	<li>Participating actively to improve safety and health in all our businesses.</li>
	<li>Collaborating in training activities aimed at controlling production processes at the level of occupational risk prevention.</li>
	<li>Promoting business participation in discussion forums aimed at reducing workplace accidents. </li>
';


 ?>