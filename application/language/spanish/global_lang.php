<?php


// ==================================================================
//
// Metas Apartados generales
//
// ------------------------------------------------------------------


$lang['global_meta_title'] = 'Norprevención – Servicio de Prevención de Riesgos Laborales';
$lang['global_meta_description'] = 'Norprevención, prevención de riesgos laborales, higiene industrial, seguridad, medicina del trabajo, formación, consultoría y psicosociología en Lugo';
$lang['global_meta_keywords'] = 'norprevencion, lugo, prevención, riesgos, laborales, higiene industrial, psicosociología, medicina del trabajo, psicosociología, formación';


$lang['servicios_meta_title'] = 'Servicios - Norprevención – Servicio de Prevención de Riesgos Laborales';
$lang['servicios_meta_description'] = 'Servicios Norprevención, prevención de riesgos laborales, higiene industrial, seguridad, medicina del trabajo, formación, consultoría y psicosociología en Lugo';
$lang['servicios_meta_keywords'] = 'servicios, norprevencion, lugo, prevención, riesgos, laborales, higiene industrial, psicosociología, medicina del trabajo, psicosociología, formación';

$lang['centros_meta_title'] = 'Red de centros - Norprevención – Servicio de Prevención de Riesgos Laborales';
$lang['centros_meta_description'] = 'Red de centros Norprevención, prevención de riesgos laborales, higiene industrial, seguridad, medicina del trabajo, formación, consultoría y psicosociología en Lugo';
$lang['centros_meta_keywords'] = 'centros, red, norprevencion, lugo, prevención, riesgos, laborales, higiene industrial, psicosociología, medicina del trabajo, psicosociología, formación';


$lang['novedades_meta_title'] = 'Novedades - Norprevención – Servicio de Prevención de Riesgos Laborales';
$lang['novedades_meta_description'] = 'Novedades Norprevención, prevención de riesgos laborales, higiene industrial, seguridad, medicina del trabajo, formación, consultoría y psicosociología en Lugo';
$lang['novedades_meta_keywords'] = 'novedades, norprevencion, lugo, prevención, riesgos, laborales, higiene industrial, psicosociología, medicina del trabajo, psicosociología, formación';


$lang['contacto_meta_title'] = 'Contacto - Norprevención – Servicio de Prevención de Riesgos Laborales';
$lang['contacto_meta_description'] = 'Contacto Norprevención, prevención de riesgos laborales, higiene industrial, seguridad, medicina del trabajo, formación, consultoría y psicosociología en Lugo';
$lang['contacto_meta_keywords'] = 'contacto, norprevencion, lugo, prevención, riesgos, laborales, higiene industrial, psicosociología, medicina del trabajo, psicosociología, formación';


$lang['empleo_meta_title'] = 'Trabaja con nosotros - Norprevención – Servicio de Prevención de Riesgos Laborales';
$lang['empleo_meta_description'] = 'Trabaja con nosotros Norprevención, prevención de riesgos laborales, higiene industrial, seguridad, medicina del trabajo, formación, consultoría y psicosociología en Lugo';
$lang['empleo_meta_keywords'] = 'empleo, trabajo, servicios, norprevencion, lugo, prevención, riesgos, laborales, higiene industrial, psicosociología, medicina del trabajo, psicosociología, formación';


$lang['presupuestos_meta_title'] = 'Presupuestos - Norprevención – Servicio de Prevención de Riesgos Laborales';
$lang['presupuestos_meta_description'] = 'Presupuestos Norprevención, prevención de riesgos laborales, higiene industrial, seguridad, medicina del trabajo, formación, consultoría y psicosociología en Lugo';
$lang['presupuestos_meta_keywords'] = 'presupuestos, servicios, norprevencion, lugo, prevención, riesgos, laborales, higiene industrial, psicosociología, medicina del trabajo, psicosociología, formación';



// ==================================================================
//
// Metas Servicios  - Subapartados
//
// ------------------------------------------------------------------



$lang['ajenos_meta_title'] = 'Ajenos de prevención - Norprevención – Servicio de Prevención de Riesgos Laborales';
$lang['ajenos_meta_description'] = 'Ajenos de prevención Norprevención, prevención de riesgos laborales, higiene industrial, seguridad, medicina del trabajo, formación, consultoría y psicosociología en Lugo';
$lang['ajenos_meta_keywords'] = 'ajenos, prevención, servicios, norprevencion, lugo, prevención, riesgos, laborales, higiene industrial, psicosociología, medicina del trabajo, psicosociología, formación';

$lang['asistencia_meta_title'] = 'Asistencia Técnica - Norprevención – Servicio de Prevención de Riesgos Laborales';
$lang['asistencia_meta_description'] = 'Asistencia Técnica Norprevención, prevención de riesgos laborales, higiene industrial, seguridad, medicina del trabajo, formación, consultoría y psicosociología en Lugo';
$lang['asistencia_meta_keywords'] = 'asistencia, tecnica, servicios, norprevencion, lugo, prevención, riesgos, laborales, higiene industrial, psicosociología, medicina del trabajo, psicosociología, formación';

$lang['consultoria_meta_title'] = 'Consultoría y Servicios Especiales - Norprevención – Servicio de Prevención de Riesgos Laborales';
$lang['consultoria_meta_description'] = 'Consultoría y Servicios Especiales Norprevención, prevención de riesgos laborales, higiene industrial, seguridad, medicina del trabajo, formación, consultoría y psicosociología en Lugo';
$lang['consultoria_meta_keywords'] = 'consultoria, especiales, servicios, norprevencion, lugo, prevención, riesgos, laborales, higiene industrial, psicosociología, medicina del trabajo, psicosociología, formación';

$lang['vigilancia_meta_title'] = 'Vigilancia de la Salud - Norprevención – Servicio de Prevención de Riesgos Laborales';
$lang['vigilancia_meta_description'] = 'Vigilancia de la Salud Norprevención, prevención de riesgos laborales, higiene industrial, seguridad, medicina del trabajo, formación, consultoría y psicosociología en Lugo';
$lang['vigilancia_meta_keywords'] = 'vigilancia, salud, servicios, norprevencion, lugo, prevención, riesgos, laborales, higiene industrial, psicosociología, medicina del trabajo, psicosociología, formación';

$lang['formacion_meta_title'] = 'Formación - Norprevención – Servicio de Prevención de Riesgos Laborales';
$lang['formacion_meta_description'] = 'Formación Norprevención, prevención de riesgos laborales, higiene industrial, seguridad, medicina del trabajo, formación, consultoría y psicosociología en Lugo';
$lang['formacion_meta_keywords'] = 'formacion, servicios, norprevencion, lugo, prevención, riesgos, laborales, higiene industrial, psicosociología, medicina del trabajo, psicosociología, formación';

// ==================================================================
//
// Metas Legal
//
// ------------------------------------------------------------------


$lang['politica_meta_title'] = 'Política de protección de datos - Norprevención – Servicio de Prevención de Riesgos Laborales';
$lang['politica_meta_description'] = 'Política de protección de datos Norprevención, prevención de riesgos laborales, higiene industrial, seguridad, medicina del trabajo, formación, consultoría y psicosociología en Lugo';
$lang['politica_meta_keywords'] = 'política, protección, datos, servicios, norprevencion, lugo, prevención, riesgos, laborales, higiene industrial, psicosociología, medicina del trabajo, psicosociología, formación';

$lang['responsabilidades_meta_title'] = 'Responsabilidades y resolución de conflictos - Norprevención – Servicio de Prevención de Riesgos Laborales';
$lang['responsabilidades_meta_description'] = 'Responsabilidades y resolución de conflictos Norprevención, prevención de riesgos laborales, higiene industrial, seguridad, medicina del trabajo, formación, consultoría y psicosociología en Lugo';
$lang['responsabilidades_meta_keywords'] = 'responsabilidades, resolución, conflictos, servicios, norprevencion, lugo, prevención, riesgos, laborales, higiene industrial, psicosociología, medicina del trabajo, psicosociología, formación';

$lang['contenido_meta_title'] = 'Contenido y uso de la web - Norprevención – Servicio de Prevención de Riesgos Laborales';
$lang['contenido_meta_description'] = 'Contenido y uso de la web Norprevención, prevención de riesgos laborales, higiene industrial, seguridad, medicina del trabajo, formación, consultoría y psicosociología en Lugo';
$lang['contenido_meta_keywords'] = 'contenido, uso, formacion, servicios, norprevencion, lugo, prevención, riesgos, laborales, higiene industrial, psicosociología, medicina del trabajo, psicosociología, formación';


// ==================================================================
//
// Home section
//
// ------------------------------------------------------------------


$lang['about_us_header'] = '¿Quiénes somos?: Breve historia de Norprevención';
$lang['about_us_p1'] = '<b>Norprevención, SL Servicio de Prevención Ajeno</b>, nace como Sociedad acreditada en Prevención de Riesgos Laborales el dos de Junio de 1999, contando, en un principio, con una Delegación Central en Lugo y una segunda Delegación en Vigo (Pontevedra) a cargo de las cuales se encontraban dos técnicos y dos administrativos.';
$lang['about_us_p2'] = 'Desde esa época hasta la actualidad Norprevención ha experimentado un gran crecimiento, contando en la actualidad con cerca de doscientos profesionales distribuidos en Delegaciones por las Comunidades Autónomas de Galicia, Castilla y León, Madrid, Asturias y Cataluña, situándonos como primer Servicio de Prevención Ajeno de Galicia en número de empresas y el primero de esta Comunidad Autónoma en conseguir la acreditación definitiva para ejercer la actividad preventiva en todo el territorio nacional.';
$lang['about_us_p3'] = 'A lo largo de estos años, hemos colaborado en infinidad de proyectos en materia preventiva:';
$lang['about_us_list'] = '
	<li>Participando de manera activa a mejorar la Seguridad y la Salud en todas nuestras empresas.</li>
	<li>Colaborando en actividades formativas encaminadas a controlar los procesos productivos al nivel de Prevención de Riesgos Laborales.</li>
	<li>Fomentando la participación empresarial en Foros de Debate dirigidos a reducir la siniestralidad laboral.</li>
';

 ?>