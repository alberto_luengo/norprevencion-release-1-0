<?php $this->load->view('common/base_begin');?>

<div  id="news">
<div class="news-main">
<section>
	<header>
		<h2>Actualidad</h2>
	</header>
	<article>
		<header>
		<!-- ONE PICTURE
		<img class="simple" src="/img/news/lorem.jpg" alt="example" />
		-->
		<h3><a href="http://medicina-del-trabajo.diariomedico.com/2012/02/27/area-cientifica/especialidades/medicina-del-trabajo/medicina-del-trabajo-da-ideas-para-ahorrar-8000-millones">
			Medicina del Trabajo da ideas para ahorrar 8.000 millones
			</a>
		</h3>
		<p>La Asociación Española de Especialistas en Medicina del Trabajo dice que las empresas y el SNS se pueden ahorrar entre 6.000 y 8.000 millones de euros fomentando la actividad de los Servicios de Prevención de Riesgos Laborales, que además aligerarían la carga en primaria.</p>
		</header>
		<p class="data">
			 Escrito el 27/02/2012
		</p>
		<footer>
		<p><span>Categoría:</span> Medicina del Trabajo</p>
		</footer>
	</article>
	
	<article>
		<header>
		<!-- TWO PICTURES
		<img class="doble" src="/img/news/lorem.jpg" alt="example" />
		<img class="doble" src="/img/news/lorem.jpg" alt="example" />
		-->
		
		<h3><a href="http://www.prevention-world.com/es/informacion-tecnica/articulos/integracion-sistema-gestion-prevencion-empresas-organizacion-y-personas-tandem-i.html">
			La integración del sistema de gestión de la prevención en las empresas. La organización y las personas, un tándem inseparable
			</a>
		</h3>
		<p>Una perfecta gestión de la actividad deberá contemplar una planificación, organización y un control de la ejecución para lograr el fin deseado, la obtención de resultados.</p>
		</header>
		<p class="data">
			 Escrito el 21/06/2012
		</p>
		<footer>
		<p><span>Categoría:</span> Información técnica</p>
		</footer>
	</article>
	
</section>

<aside class="secondary">
	<!-- NOTHING TO SEE HERE YET -->
</aside>
</div>
</div>

<?php $this->load->view('common/base_end');?>