<?php $this->load->view('common/base_begin');?>
<div id="workus">
	<div>
	<section>
		<header>
			<h2>Empleo</h2>
		</header>

	<?php if(!$msn){ ?>
		 <form id="form" enctype="multipart/form-data" action="/<?php echo LANG;?>/empleo" method="post">
			<fieldset>
				<legend>Trabaja con nosotros</legend>
				<div>
					<label for="name">Nombre Apellidos</label>
					<input name="name" id="name" value="" type="text">
				</div>

				<div>
					<label for="area">Área</label>
					<select id="area" name="area">
						<option value="Administración">Administración</option>
						<option value="Comercial">Comercial</option>
						<option value="Técnico">Técnico</option>
						<option value="Vigilancia de la salud">Vigilancia de la salud</option>
					</select>
				</div>

				<div class="doble">
					<label for="years">Años de experiencia</label>
					<input name="years" id="years" value="" type="text">
				</div>
				<div class="doble">
					<label for="mail">E-mail de contacto</label>
					<input name="mail" id="mail" value="" type="text">

				</div>

				<div>
					<label for="comment">Comentarios</label>
					<textarea name="comment" id="comment" rows="5" cols="20"></textarea>
				</div>

				<div>
					<label for="userfile">Adjuntar CV</label>
					<input type="file" name="userfile" id="userfile">
				</div>

				<div class="choose">
						<input type="checkbox" checked="checked" disabled="disabled">
						<a target="_blank" href="/<?php echo LANG;?>/legal/politica">Acepto las condiciones legales</a>
				</div>
				
				<div>
					<input value="ENVIAR" type="submit">
				</div>
			</fieldset>
		</form>

		<?php }else{ ?>

			<p class="accepted">
				<?php echo $msn; ?>
			</p>

		<?php } ?>
	</section>
	<aside>
		<h3>¿Quieres trabajar con nosotros?</h3>
			<p>
			Si quieres trabajar con nosotros, puedes ponerte en contacto con nosotros mediante este formulario, 
		enviando un e-mail a <?php echo safe_mailto('rrhh@norprevencion.com','rrhh@norprevencion.com');?> o llamando al teléfono <b>902 020 880</b>
			</p>
		</aside>
	</div>

</div>
<?php $this->load->view('common/base_end');?>