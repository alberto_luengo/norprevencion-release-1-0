<?php $this->load->view('common/base_begin');?>

<div id="services">
	<section>
		<header>
			<h2>Vigilancia de la Salud</h2>
		</header>
		<div class="services-salud">
			
			
			<div>
            	<h3>¿En qué consiste?</h3>
				<p>La vigilancia de la salud es el conjunto de actuaciones sanitarias aplicadas a los 
trabajadores con la ﬁnalidad de evaluar su estado de salud en relación a los riesgos 
laborales a los que están expuestos. </p><p>
Se realiza por medio de reconocimientos médicos especíﬁcos a dichos riesgos, 
siendo sus contenidos y periodicidad adaptados a cada puesto de trabajo. Además de 
prevenir la posible aparición de enfermedades laborales incipientes, los resultados de 
estos exámenes médicos sirven también para evaluar la aptitud para el desempeño de la 
actividad laboral de cada trabajador. 

				</p>
            </div>
            <div>
            	<h3>¿Por qué lo necesita?</h3>
				<p>
				La vigilancia de la Salud es voluntaria para el trabajador salvo que concurra alguna 
de las siguientes circunstancias: 
				</p>
				<ul>
					<li>La existencia de una disposición legal con relación a la protección de riesgos especíﬁcos 
y actividades de especial peligrosidad (ver el Art. 196 de la Ley General de la Seguridad 
Social)</li>
					<li>Que los reconocimientos sean indispensables para evaluar los efectos de las condiciones 
de trabajo sobre la salud de los trabajadores</li>
					<li>Que el estado de la salud del trabajador pueda constituir un peligro para él mismo o para 
terceros</li>
				</ul>
            </div>
             <div>
            	<h3>¿Qué puede hacer Norprevención por usted?</h3>
				<p>
				Principalmente, nos encargaremos del estudio de la Evaluación de Riesgos y sus 
actualizaciones y medidas de Vigilancia de la Salud a adopta y de la realización de los 
exámenes de salud individualizados a cada uno de sus trabajadores, entre otras 
actuaciones.
				</p>
        	</div>
		
		<p class="down-detail"><a href="#">Más información</a></p>
		</div>
	</section>
</div>

<?php $this->load->view('common/base_end');?>