<?php $this->load->view('common/base_begin');?>

<div id="services">
	<section>
		<header>
			<h2>Formación</h2>
		</header>
		
		
        <div  class="services-forma">
        
        
           <div>
            	<h3>¿En qué consiste?</h3>
				<p>En la actualidad nos encontramos en que todo el mundo puede producir de todo, 
en cualquier lugar y calidad y con unos costes similares, la organización que pretende 
apostar por competir en esas áreas lo tiene bastante complicado pues aun y en el caso de 
que logre tener en algún momento una ventaja <b>competitiva</b> en esos factores, esa ventaja 
competitiva será tan ligera, tan nimia y especialmente tan poco estable que en la 
mayoría de casos no merecerá apostar por ahí, pues fácilmente podrá ser rebasada y 
superada por sus competidores. 
				</p>
            </div>
            <div>
            	<h3>¿Por qué lo necesita?</h3>
				<p>
				Una organización formada no solo nos aportara una ventaja competitiva por el 
conocimiento que adquiere de cara al exterior, de ﬁlosofía de <b>mejora continua</b> no sólo 
nos aportara ser más competitivos a la empresa por saber más que nuestra competencia, 
sino por saber aplicar también ese saber a la optimización y mejora de todos nuestros 
propios procesos internos. 
				</p>
            </div>
             <div>
            	<h3>¿Qué puede hacer Norprevención por usted?</h3>
				<p>En Norprevención siempre hemos apostado por la idea de que la <b>capacitación</b> de 
todos los eslabones que forman una organización es una condición necesaria para que la 
misma pueda sacar el máximo rendimiento de sí misma y para que encuentre su camino 
hacia la <b>excelencia</b> . Nuestras áreas de actuación son las siguientes: 
				</p>
				<ul>
					<li>Oferta formativa propia</li>
					<li>Ampilo catálogo de cursos para casi todos los sectores laborales </li>
					<li>Formación boniﬁcada</li>
					<li>Formación FLC (Fundación Laboral de la Construcción) </li>
					<li>Formación in-company</li>
				</ul>
        	</div>
        	
        	<p class="down-detail"><a href="#">Más información</a></p>
        	
        </div>
	</section>
</div>

<?php $this->load->view('common/base_end');?>