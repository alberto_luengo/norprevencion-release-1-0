<?php $this->load->view('common/base_begin');?>

<div id="services">
	<section>
		<header>
			<h2>Consultoría y Servicios Especiales</h2>
		</header>
		
		
        <div  class="services-consult">

			<div>
            	<h3>¿En qué consiste?</h3>
				<p>
				El asesoramiento personalizada y el empeño por la mejora siempre han sido 
valores prioritarios para Norprevención, por lo que ofrecer servicios como implantaciones de sistemas de calidad que le permitan escalar un peldaño en la calidad de su 
organización o llevar a cabo tareas de consultoría sobre los posibles puntos débiles de los 
procesos que sigue su empresa constituye uno de nuestros puntos fuertes como Servicio 
de Prevención Ajeno.
				</p>
            </div>
            <div>
            	<h3>¿Por qué lo necesita?</h3>
				<p>
				Las exigencias tanto internas como externas de las organizaciones cada vez son 
mayores.  Internamente,  la organización está obligada a mejorar la calidad del 
producto/servicio, la reducción de costes, mejorar la infraestructura de la organización y 
mejorar la satisfacción de los empleados. 
				</p>
				<p>
				
Asimismo se ha hecho imprescindible seguir la corriente del mercado, promocionar 
la imagen corporativa, desarrollar nuevos mercados, incrementar la competitividad 
internacional e incrementar la cuota de mercado. 

				</p>
            </div>
             <div>
            	<h3>¿Qué puede hacer Norprevención por usted?</h3>
            	<!--
				<p>
				Norprevención pone a su disposición su servicio de consultoría especializada en 
Prevención de Riesgos Laborales. 
</p>
-->
<p>
Para desarrollar este objetivo establecemos y desarrollamos una metodología de 
actuación que permite evaluar con ﬁabilidad los riesgos existentes en el lugar de trabajo, 
rodeándonos para ello de un equipo de profesionales homologados para realizar labores 
como Técnicos Superiores en Prevención de Riesgos Laborales y Coordinadores de 
Seguridad y Salud en las obras de construcción con una amplia experiencia profesional. 
</p><p>
Realizamos un minucioso estudio dirigido a evaluar todos los factores de riesgo 
para: 
				</p>
				
				<ul>
					<li>Eliminar y/o reducir el riesgo</li>
					<li>Controlar periódicamente las condiciones de trabajo</li>
					<li>Interponer las medidas correctoras necesarias</li>
					<li>Impllantar distintos sistemas de calidad</li>
				
				</ul>
        	</div>
        	<p class="down-detail"><a href="#">Más información</a></p>
        </div>
        
	</section>
</div>

<?php $this->load->view('common/base_end');?>