<?php $this->load->view('common/base_begin');?>

<div id="services">
	<section>
		<header>
			<h2>Servicios de Asistencia Técnica a Construcción</h2>
		</header>
		
        <div  class="services-assist">
			<div>
            	<h3>¿En qué consiste?</h3>
            	<p>
            	
            	En un servicio de asistencia técnica a construcción se ejecutan las distintas actividades 
necesarias para cumplir todos los controles de calidad en cuanto a utilización de equipos 
de trabajo, protecciones colectivas e individuales y medidas de seguridad.
</p><p>Asimismo, se 
realizan comprobaciones y mediciones de hormigones, tierras, etc. 
            	</p>
            	<!--
				<p>En visita a obra se comprueban: 
				</p>
				<ul>
					<li>Medidas de seguridad</li>
					<li>Equipos de trabajo</li>
					<li>Utilización de equipos de protección individual por parte de los trabajadores</li>
					<li>Protecciones colectivas (barandillas, lineas de vida, etc) </li>
					<li>Control de calidad (ensayos de hormigones, acompañando a personal de laboratorio a 
recoger probetas de hormigón, ensayos de densidad de terraplenes, tierras, ferrallas). </li>
					<li>Comprobación y medición </li>
					<li>...(COMPLETAR) </li>
				</ul>
				-->
				
            </div>
            <div>
            	<h3>¿Por qué lo necesita?</h3>
				<p>
				Desde el promotor hasta el contratista pasando por el personal de obra, todas las 
personas involucradas en un proyecto de construcción deben de ser conscientes y estar 
informados de la importancia de la labor que están desempeñando, de su propia 
protección y de la de sus compañeros, y de la ﬁabilidad de los materiales con los que 
están trabajando.
				</p>
            </div>
             <div>
            	<h3>¿Qué puede hacer Norprevención por usted?</h3>
				<p>
				Nos ocuparemos de la coordinación de seguridad y salud en todas las fases del 
proyecto y su ejecución, de la elaboración de los planes de seguridad, salud y trabajo y de 
la asistencia técnica en materia de seguridad y salud, entre otras actividades.
				</p>
        	</div>
        	<p class="down-detail"><a href="#">Más información</a></p>
		</div>
		
	</section>
</div>

<?php $this->load->view('common/base_end');?>