<?php $this->load->view('common/base_begin');?>

<div id="services">
	<section>
		<header>
			<h2>
                Servicio de prevención ajeno
            </h2>
		</header>
		
        <div  class="services-prevention">
            <div>
            	<h3>¿En qué consiste?</h3>
				<p>
				Los Servicios de Prevención Ajenos (SPA) son empresas especializadas en el área 
				de prevención de riesgos laborales que ofrecen a otras empresas sus servicios para el 
				desarrollo de las actividades preventivas exigidas legalmente a éstas. 
				</p>

            </div>
            
             <div>
            	<h3>¿Por qué lo necesita?</h3>
				<p>
				Porque la Ley así lo estipula  y porque mediante nuestro trabajo, le ayudaremos a 
aumentar la productividad y competitividad Evita sanciones y mejorar la imagen de la 
marca y la valoración de la compañía. 

				</p>

            </div>
            
             <div>
            	<h3>¿Qué puede hacer Norprevención por usted?</h3>
				<p>
				Nos ocuparemos de la gestión <b>integral</b>  de todo lo relacionado con la actividad 
preventiva, llevando un seguimiento personal de sus necesidades, asesorándolo y realizando todas las 
actividades necesarias para que cumpla la Ley y ahorre costes.
				</p>
				<!--
			<ol>
                <li>Diseño y elaboración de Plan de Prevención según lo establecido en la Ley 54/2003 y en la Ley 31/95.</li>
                <li>Evaluación de los factores de riesgo que puedan afectar a la seguridad y salud de los trabajadores en los términos previstos en el artículo 16 de la Ley 31/1995 y modificaciones reglamentarias posteriores.</li>
                <li>Determinación de las prioridades en la adopción de medidas preventivas adecuadas y vigilancia de su eficacia.</li>
                <li>Planificación de dichas medidas, estableciendo fechas tope y puntos críticos en su aplicación.</li>
                <li>Elaboración de documentación informativa a los trabajadores en materia de seguridad y salud.</li>
                <li>Elaboración de normas de actuación en caso de emergencia en función de lo estipulado en el artículo 20 de la Ley de Prevención de Riesgos Laborales (Ley 31/95).</li>
                <li>Diseño e implantación de programas de formación e información a los trabajadores, en función de lo estipulado en los artículos 18 y 19 de la Ley de PRL (Ley 31/95).</li>
                <li>Elaboración de toda la documentación obligatoria en función del artículo 23 de la Ley de Prevención Riesgos Laborales (Ley 31/95).</li>
                <li>Especificación de protocolos y pruebas especiales a aplicar de cara a la vigilancia de la salud de los trabajadores, de acorde a lo dispuesto en el artículo 22 de la LPRL (Ley 31/95).</li>
                <li>Asesoramiento en materia preventiva en todas las dudas o cuestiones que sean necesarias.</li>
                <li>Investigación de accidentes ocurridos en la empresa y enfermedades profesionales.</li>
                <li>Elaboración de memoria anual, indicando actuaciones realizadas, así como grado de avance en materia de seguridad y salud.</li>
            </ol>
            -->
            </div>
            
            <p class="down-detail"><a href="#">Más información</a></p>
            
            
        </div>
        
	</section>
</div>

<?php $this->load->view('common/base_end');?>
