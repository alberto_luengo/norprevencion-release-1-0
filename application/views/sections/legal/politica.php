<?php $this->load->view('common/base_begin');?>

<div id="legal">
	<section>
		<header>
			<h2>Política de protección de datos</h2>
		</header>

		<p>
		
		Todos aquellos datos que pudieran ser recabados por <b>Norprevencion Servicio de
Prevención Ajeno, S.L.</b> a través de esta web serán incorporados a los ficheros de los
que esta entidad es responsable y que se encuentran debidamente inscritos en el
Registro General de Protección de Datos de la Agencia Española de Protección de Datos,
con el objeto ser tratados según los fines establecidos en relación a los servicios
prestados.
</p>
<p>
Asimismo este Servicio de Prevención Ajeno manifiesta el estricto cumplimiento de
todas y cada una de las medidas a las que, conforme a lo dispuesto en la Ley Orgánica
de Protección de Datos 15/1999 de 13 de diciembre y Reglamento de Medidas de
Seguridad Real Decreto 1720/2007 de 21 de diciembre, esta obligación en virtud de su
condición.
</p>
<p>
Para el ejercicio de los derechos de acceso, cancelación, rectificación u oposición,
deberá remitir la solicitud correspondiente al derecho que se pretenda ejercitar
acompañada del DNI y dirección a efectos de notificaciones, a nuestro Dpto. de Derechos
ARCO en la Dirección: C/ CATASOL, 11, 27002 - LUGO (LUGO).
</p>

	</section>
</div>

<?php $this->load->view('common/base_end');?>