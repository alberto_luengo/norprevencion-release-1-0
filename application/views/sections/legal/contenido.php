<?php $this->load->view('common/base_begin');?>

<div id="legal">
	<section>
		<header>
			<h2>Contenido y uso de la web</h2>
		</header>
<ol>
<li><b>Norprevención Servicio de Prevención Ajeno, S.L.</b> es titular de los derechos de
propiedad intelectual e industrial reflejados en el sitio <a href="http://www.norprevencion.com">norprevencion.com</a>, así
como de todos los elementos contenidos en el mismo. El usuario se compromete a
respetar dichos derechos.</li>
<li>La utilización o la publicación, parcial o total, con fines comerciales, de cualquier
contenido de la web <a href="http://www.norprevencion.com">norprevencion.com</a> está estrictamente prohibida sin
autorización previa de Norprevención Servicio de Prevención Ajeno, S.L. Está
prohibido, de igual modo, modificar o suprimir material o contenidos cuyos derechos se
reserva Norprevención Servicio de Prevención Ajeno, S.L.</li>
<li>Los encargados de sitios web que creen enlaces con la web de
<a href="http://www.norprevencion.com">norprevencion.com</a> deben informar a Norprevención Servicio de Prevención Ajeno, S.L.
por correo electrónico a la siguiente dirección: <?php echo safe_mailto('info@norprevencion.com','info@norprevencion.com');?>. Dichas
conexiones no implicará ningún tipo de asociación o participación con las entidades
conectadas. Norprevención Servicio de Prevención Ajeno, S.L. se reserva el derecho
a denegar dicho acceso en cualquier momento.
En el caso de que cualquier usuario de la web <a href="http://www.norprevencion.com">norprevencion.com</a>
entendiese que el contenido o los servicios prestados por las páginas enlazadas son
ilícitos, vulneran valores o principios constitucionales, o lesionan bienes o derechos del
propio usuario o de un tercero, se ruega que lo comuniquen a la siguiente dirección:
<?php echo safe_mailto('info@norprevencion.com','info@norprevencion.com');?></li>
<li>En ningún caso Norprevención Servicio de Prevención Ajeno, S.L., sus
suministradores o los terceros mencionados en esta página serán responsables por daños
de cualquier tipo (incluyendo, entre otros, los daños resultantes de pérdidas de beneficios,
pérdida de datos o interrupción de negocio) que resulten del uso, imposibilidad de uso o
de los resultados del uso de esta página, cualquier página web conectada a ésta o de los
materiales o información contenida en cualesquiera de dichas páginas, ya se base dicha
responsabilidad en garantía, contrato, culpa o negligencia, o cualquier otra teoría legal, y
con independencia de que se haya avisado o no sobre la posibilidad de tales daños. Si la
utilización por el usuario de los materiales o información de esta página trae consigo la
necesidad de prestar un servicio, reparar o corregir el equipo o datos, dicho usuario será
responsable de tales costes.</li>
</ol>

	</section>
</div>

<?php $this->load->view('common/base_end');?>