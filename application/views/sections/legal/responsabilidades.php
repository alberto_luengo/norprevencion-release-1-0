<?php $this->load->view('common/base_begin');?>

<div id="legal">
	<section>
		<header>
			<h2>Responsabilidades y resolución de conflictos</h2>
		</header>

	<p>
	El uso de la web <a href="http://www.norprevencion.com">norprevencion.com</a> propiedad de <b>Norprevención
Servicio de Prevención Ajeno, S.L.</b>, implica la aceptación de sus Términos de Uso,
quedando expresamente exonerada la misma de cualquier perjuicio que pudiera
ocasionarse a terceros por el incumplimiento de los usuarios de las condiciones
establecidas en la presente nota.
</p><p>
En caso de conflicto, la normativa de aplicación será la legislación española vigente
y cualquier controversia será sometida a los Juzgados y Tribunales de Lugo.
	</p>

	</section>
</div>

<?php $this->load->view('common/base_end');?>