<?php $this->load->view('common/base_begin');?>
<div id ="contact">
	<div>
		<section>
			<header>
				<h2>Contacto</h2>
			</header>

				<?php if(!$msn){ ?>

				 <form id="form" action="/<?php echo LANG;?>/contacto" method="post">
						<fieldset>
							<legend>Contacta con nosotros</legend>
							<div class="doble">
								<label for="name">Nombre</label>
								<input name="name" id="name" value="" type="text">
							 </div>
							 <div class="doble">
								<label for="mail">E-mail</label>
								<input name="mail" id="mail" value="" type="text">
							 </div>
							 <div>
							 	<label for="reason">Motivo</label>
								<select id="reason" name="reason">
									<option value="Consulta">Consulta</option>
									<option value="Reclamación">Reclamación</option>
								</select>
							 </div>
							<div>
								<label for="comment">Comentarios</label>
								<textarea name="comment" id="comment" rows="5" cols="20"></textarea>
							</div>
							<div>
								<input value="ENVIAR" type="submit">
							</div>
						</fieldset>
					</form>

					<?php }else{ ?>

					<p class="accepted">
						<?php echo $msn; ?>
					</p>

					<?php } ?>

		</section>
		<aside>
		<h3>¿Quieres contactar con nosotros?</h3>
			<p>
			Si deseas ponerte en contacto con nosotros o realizar alguna reclamación, contacta con nosotros mediante
			este formulario o bien mediante el siguiente e-mail: <?php echo safe_mailto('info@norprevencion.com','info@norprevencion.com');?>
			</p>
		</aside>
	</div>
</div>
<?php $this->load->view('common/base_end');?>