<?php $this->load->view('common/base_begin'); ?>



<!-- TEST SUBMENUS
<ul class="wrapper tabs tab-top">
    <li><a href="#">Que ofrecemos</a></li>
    <li><a href="#">Lorem ipsum</a></li>
    <li><a href="#">Dolor sit amet</a></li>
</ul>
-->


<section class="detailpromo">
    <div class="main">
        <div>
            <h3>¿Qué es un Servicio de Prevención? </h3>
            <p>
Una empresa que tiene como objetivo dar soporte al resto de empresas con el ﬁn
de garantizar la adecuada protección de la seguridad y salud de los trabajadores
mediante la aplicación de las oportunas medidas y el desarrollo de las actividades
necesarias para la prevención de los riesgos derivados del trabajo.            </p>
        </div>
        <div>
            <h3>¿Por qué necesita un contrato de prevención? </h3>

            <p>
Porque es obligatorio por Ley para todas las empresas y autónomos, le ayudará a
ser más competitivo y le ayudará a reducir costes futuros.			</p>
        </div>

        <div>
            <h3>¿Cómo le podemos ayudar? </h3>

            <p>
Como SPA (Servicio de Prevención Ajena) nos ocuparemos de la gestión integral
de la prevención de su organización, con un equipo de profesionales cualiﬁcados a su
servicio.			</p>
        </div>
    </div>
</section>



<section id="about">

	<!--
    <img src="/img/norprevencion/04.jpg" alt="norprevencion" width=1032 />
	-->
    <header>
        <h2><?php echo $this->lang->line('about_us_header');?></h2>
    </header>
    <div>
        <p>
            <?php echo $this->lang->line('about_us_p1');?>
        </p>
        <p class="tabula">
            <?php echo $this->lang->line('about_us_p2');?>
        </p>
    </div>
    <div>
        <p>
            <?php echo $this->lang->line('about_us_p3');?>
        </p>
        <ul>
          <?php echo $this->lang->line('about_us_list');?>
        </ul>
    </div>
</section>

<section class="detail">
    <div class="main">
        <div>
            <!--
            <h3>Orientación al cliente</h3>
            -->
            <h3>Misión</h3>

            <img src="/img/norprevencion/02.jpg" alt="seguridad" />
            <p>
Nuestra misión es el promover la seguridad y la salud integral de los
trabajadores, mediante la aplicación de medidas y el desarrollo de las actividades
necesarias para la prevención de riesgos derivados del trabajo, de forma participativa y
eﬁcaz.
            </p>
        </div>

        <div>
        	<!--
            <h3>Calidad de servicio</h3>
            -->
            <h3>Visión</h3>

            <img src="/img/norprevencion/03.jpg" alt="seguridad" />
            <!--
            <p>
            Desde un principio la Calidad en la atención a sus clientes ha sido uno de los pilares en los que Norprevención ha apoyado la gestión de sus empresas.
	</p>
            <p>
            Para garantizar ese nivel de calidad NORPREVENCIÓN ha implantado satisfactoriamente un â€œSistema de Aseguramiento de la Calidad Internoâ€ desarrollado en el Manual de Calidad de la compañí­a. El objeto de su elaboración es definir, bajo forma documentada, todas las condiciones particulares de organización, personal y procedimientos de actuación interna, que garanticen que los servicios de asistencia tí©cnica, objeto de nuestros conciertos de actividades, se llevan a cabo cumpliendo con un nivel de calidad máximo.
	</p>
	-->
            <p>
Como Servicio de Prevención de Riesgos Laborales hemos consolidado una
visión humana e innovadora sobre la prevención y un compromiso de ofrecer siempre lo
mejor a nuestros clientes, brindando asesoramiento integral en las cuatro especialidades:</p>
	<ul>
		<li>Seguridad en el Trabajo</li>
		<li>Vigilancia de la Salud</li>
		<li>Higiene Industrial y Ergonomía</li>
		<li>Psicosociología Aplicada</li>
	</ul>

        </div>

        <div>
        	<!--
            <h3>Atención al Cliente</h3>
            -->
            <h3>Valores</h3>

            <img src="/img/norprevencion/01.jpg" alt="seguridad"  />
            <ul>
				<li>Espíritu de equipo y trabajo multidisciplinar. </li>
				<li>Personas con competencia técnica, formación y capacitación acreditada.</li>
				<li>Atención personalizada y conﬁdencial a los trabajadores en todo aquello relacionado con
	su actividad profesional. </li>
				<li>Asesoramiento a los órganos directivos, delegados de prevención y a otras entidades
	relacionadas con la Prevención de Riesgos Laborales.</li>
				<li>Conﬁanza y credibilidad generada en los trabajadores.
				<!--, a través del trabajo realizado por
				las personas integrantes de la Unidad.
				-->
				</li>
            </ul>

        </div>
    </div>
</section>





<?php $this->load->view('common/base_end'); ?>