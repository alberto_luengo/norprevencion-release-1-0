<?php $this->load->view('common/base_begin'); ?>

<div id="network">
    <section>
    	
        <header>
            <h2>Red de Centros de Norprevención</h2>
        </header>
            <p class="help">Pulsa sobre las provincias para obtener información sobre nuestros centros</p>
		<img src="/img/norprevencion/center-map.png" alt="Mapa rede de centros" width="688" height="536" usemap="#Map" />
		<!--
        <map name="Map" id="Map">
          <area shape="circle" onclick="onCenter('coru');return false;" href="#" coords="70,25,28" />
          <area shape="circle" onclick="onCenter('santiago');return false;" href="#" coords="34,67,28" />
          <area shape="circle" onclick="onCenter('vigo');return false;" href="#" coords="33,137,28" />
          <area shape="circle" onclick="onCenter('estrada');return false;" href="#" coords="76,104,28" />
          <area shape="circle" onclick="onCenter('ourense');return false;" href="#" coords="116,140,28" />
          <area shape="circle" onclick="onCenter('lugo');return false;" href="#" coords="124,74,28" />
          <area shape="circle" onclick="onCenter('asturias');return false;" href="#" coords="202,38,28" />
          <area shape="circle" onclick="onCenter('leon');return false;" href="#" coords="218,102,28" />
          <area shape="circle" onclick="onCenter('bcn');return false;" href="#" coords="601,217,28" />
          <area shape="circle" onclick="onCenter('madrid');return false;" href="#" coords="327,262,28" />
        </map>
        -->
        
        <!--
        <map name="Map" id="Map">
		<area shape="poly"		onclick="onCenter('lugo');return false;" coords="68,84,68,76,69,68,70,45,76,28,83,6,97,13,112,19,108,29,114,45,123,47,118,56,123,64,114,75,113,88,108,99,97,93,81,98,69,84" href="#" alt="lugo" />
		<area shape="poly" 		onclick="onCenter('asturias');return false;" coords="113,18,153,15,168,17,175,10,184,17,201,20,239,29,276,25,287,22,315,33,329,26,348,34,365,34,377,29,379,35,360,60,354,63,345,87,337,91,331,79,325,81,312,70,316,61,308,43,300,48,285,45,269,59,275,68,267,70,256,67,247,54,229,55,226,44,220,42,210,50,189,52,177,57,160,49,125,62,125,43,114,39,109,28" href="#" alt="asturias" />
		<area shape="poly" 		onclick="onCenter('leon');return false;" coords="279,160,289,149,303,137,323,120,307,107,309,85,316,78,301,64,310,61,304,50,296,52,282,53,277,59,275,70,263,74,250,81,252,98,255,115,264,127,264,139,262,152" href="#" alt="leon" />
		<area shape="poly" 		onclick="onCenter('leon');return false;" coords="194,182,204,178,204,127,200,121,198,110,218,105,224,55,226,46,212,53,181,59,163,55,141,63,129,66,118,77,118,89,126,94,131,106,119,119,123,125,138,127,134,148,152,149,154,161,143,171,168,178,184,176" href="#" alt="leon" />
		<area shape="poly" 		onclick="onCenter('ourense');return false;" coords="61,137,71,133,94,136,114,126,110,117,121,107,119,94,106,106,97,98,80,100,65,87,49,90,54,114,64,122,54,130" href="#" alt="ourense" />
		<area shape="poly" 		onclick="onCenter('coru');return false;" coords="19,84,28,78,53,66,69,64,70,38,84,4,66,5,47,20,53,32,45,30,18,33,4,49,4,58,11,70,14,85" href="#" alt="coruña" />
		
		<area shape="poly" 		onclick="onCenter('ponte');return false;" coords="23,129,40,122,52,116,44,85,61,85,66,68,40,72,23,88,22,100,18,118" href="#" alt="pontevedra" />
		
		<area shape="poly" 		onclick="onCenter('bcn');return false;" coords="427,396,448,367,481,341,463,325,454,286,498,219,516,212,502,203,528,185,579,172,588,158,631,129,629,96,602,100,567,101,551,93,533,77,503,68,508,126,488,145,489,168,485,182,479,213,458,209,452,223,435,247,422,266,411,260,392,289,412,302,407,319,421,327,420,345,414,359,420,382" href="#" alt="barcelona" />
		<area shape="circle" 	onclick="onCenter('bcn');return false;" coords="568,519,14" href="#" alt="barcelona" />
		<area shape="poly" 		onclick="onCenter('bcn');return false;" coords="630,509,648,475,657,472,657,500" href="#"  alt="barcelona" />
		<area shape="poly" 		onclick="onCenter('bcn');return false;" coords="650,463,671,450,676,441,673,460,661,469" href="#" alt="barcelona" />
		<area shape="poly" 		onclick="onCenter('madrid');return false;" coords="144,536,177,513,137,461,105,436,98,441,89,413,122,377,102,357,116,319,88,281,115,277,117,255,116,245,125,233,122,194,143,172,169,183,185,179,208,182,207,126,205,112,224,107,225,57,243,56,261,74,248,80,250,113,260,129,257,156,281,163,324,123,315,105,317,88,329,85,339,96,350,91,384,33,393,49,407,53,454,69,498,77,504,124,485,146,481,185,480,205,458,203,430,257,407,257,389,290,403,305,408,323,419,331,413,363,428,399,442,410,392,415,389,430,357,468,342,461,327,472,301,467,261,469,235,482,210,487,188,513,250,531,334,525,350,534" href="#" alt="madrid" />
		<area shape="circle" 	onclick="onCenter('madrid');return false;" coords="549,331,17" href="#" alt="madrid" />
		<area shape="circle" 	onclick="onCenter('madrid');return false;" coords="617,286,26" href="#" alt="madrid" />
		<area shape="circle" 	onclick="onCenter('madrid');return false;" coords="670,267,17" href="#" alt="madrid" />
		<area shape="poly" 		onclick="onCenter('madrid');return false;" coords="462,465,460,478,456,487,482,502,507,493,531,482,541,484,519,514,503,502,487,513,452,524,446,534,436,530,449,522,475,511,450,489,443,468" href="#" alt="madrid" />
		</map>
		-->
		
		
<map name="Map" id="Map">
<area shape="poly" 					onclick="onCenter('lugo',1);return false;" coords="68,84,68,76,69,68,70,45,76,28,83,6,97,13,112,19,108,29,114,45,123,47,118,56,123,64,114,75,113,88,108,99,97,93,81,98,69,84" href="#" alt="Lugo" />
<area shape="poly"		coords="282,155,292,144,306,132,320,123,309,111,312,80,319,73,304,59,313,56,307,45,299,47,285,48,276,56,276,56,275,66,253,76,255,93,258,110,267,122,267,134,265,147" href="#" onclick="onCenter('leon','Burgos');return false;" />
<area shape="poly" 					onclick="onCenter('ourense',1);return false;" coords="61,137,71,133,94,136,114,126,110,117,121,107,119,94,106,106,97,98,80,100,65,87,49,90,54,114,64,122,54,130" href="#" alt="Ourense" />
<area shape="poly" 					onclick="onCenter('coru',1);return false;" coords="19,84,28,78,53,66,69,64,70,38,84,4,66,5,47,20,53,32,45,30,18,33,4,49,4,58,11,70,14,85" href="#" alt="A Coruña" />
<area shape="poly" 					onclick="onCenter('ponte',1);return false;" coords="23,129,40,122,52,116,44,85,61,85,66,68,40,72,23,88,22,100,18,118" href="#" alt="Pontevedra" />
<area shape="circle" 	coords="568,519,14" href="#" onclick="onCenter('madrid','Las Palmas');return false;" />
<area shape="poly" 		coords="630,509,648,475,657,472,657,500" href="#" onclick="onCenter('madrid','Las Palmas');return false;" />
<area shape="poly" 		coords="650,463,671,450,676,441,673,460,661,469" href="#" onclick="onCenter('madrid','Las Palmas');return false;" />
<area shape="circle" 	coords="549,331,17" href="#" onclick="onCenter('bcn','Baleares');return false;" />
<area shape="circle" 	coords="617,286,26" href="#" onclick="onCenter('bcn','Baleares');return false;" />
<area shape="circle" 	coords="670,267,17" href="#" onclick="onCenter('bcn','Baleares');return false;" />
<area shape="poly"		coords="462,465,460,478,456,487,482,502,507,493,531,482,541,484,519,514,503,502,487,513,452,524,446,534,436,530,449,522,475,511,450,489,443,468" href="#" onclick="onCenter('madrid','Sta Cruz');return false;" />
<area shape="poly" 					onclick="onCenter('asturias',1);return false;"coords="113,18,163,17,175,10,181,18,196,17,238,29,239,35,224,43,221,38,210,48,189,53,183,52,176,58,166,51,159,52,148,53,142,60,126,62,120,56,127,46,114,43,110,30" href="#" alt="Asturias" />
<area shape="poly" 		coords="222,45,240,37,241,30,258,28,278,22,278,27,288,20,311,32,304,35,297,39,294,46,283,44,270,59,273,64,261,70,253,57,243,51,228,55" href="#" onclick="onCenter('asturias','Cantabria');return false;" />
<area shape="poly" 		coords="339,51,333,55,319,50,321,42,313,46,303,42,308,37,318,26,328,25,348,34,344,49" href="#" onclick="onCenter('asturias','Bizcaia');return false;" />
<area shape="poly" 		coords="337,58,340,53,346,50,351,35,367,35,376,28,380,36,362,58,356,59,355,64,347,58" href="#" onclick="onCenter('asturias','Guipuzcoa');return false;" />
<area shape="poly" 		coords="346,90,345,83,350,80,356,66,345,60,335,59,331,56,318,52,317,47,310,48,314,59,313,63,322,76,326,83,338,90" href="#" onclick="onCenter('asturias','Álava');return false;" />
<area shape="poly" 					onclick="onCenter('leon',1);return false;" coords="127,106,126,98,119,89,114,89,117,76,126,65,143,62,148,55,166,54,176,60,183,54,190,55,212,50,220,40,227,56,223,66,219,73,224,78,223,90,218,105,212,102,204,110,200,107,199,121,194,125,192,118" href="#" alt="Leon" />
<area shape="poly" 		coords="217,128,221,119,220,114,215,114,216,107,221,106,223,79,219,73,229,57,245,54,259,71,251,79,253,96,256,114,265,122,266,132,265,139,242,139,244,133,235,136,234,131,229,136,223,129" href="#" onclick="onCenter('leon','Palencia');return false;" />
<area shape="poly" 		coords="118,126,115,119,123,108,128,108,191,119,194,127,199,123,203,126,204,137,198,141,207,157,201,161,201,183,192,180,183,176,171,175,169,181,164,178,158,178,143,171,154,153,147,145,140,147,138,127" href="#" onclick="onCenter('leon','Zamora');return false;" />
<area shape="poly" 		coords="120,240,125,234,127,199,118,190,125,189,135,176,144,172,170,182,173,176,201,185,202,180,210,183,209,189,213,193,192,221,189,231,181,231,181,239,175,235,169,239,153,225,133,240" href="#" onclick="onCenter('madrid','Salamanca');return false;" />
<area shape="poly" 		coords="206,179,217,182,224,185,233,181,240,172,242,165,267,155,265,151,264,141,241,140,241,136,235,138,233,134,229,137,223,131,216,130,219,118,215,115,216,108,211,104,204,111,199,114,200,121,206,126,207,138,201,141,209,157,205,162,203,175,207,175" href="#" onclick="onCenter('leon','Valladolid');return false;" />
<area shape="poly" 		coords="184,240,190,244,199,240,198,250,205,253,213,248,218,251,225,238,231,239,231,245,238,244,240,235,247,231,254,217,247,217,232,184,224,186,212,183,209,189,216,193,194,221" href="#" onclick="onCenter('madrid','Ávila');return false;" />
<area shape="poly" coords="252,214,258,212,264,206,270,202,273,192,280,189,288,180,307,171,288,153,278,160,268,151,268,158,244,167,241,173,235,183,244,208" href="#" onclick="onCenter('madrid','Segovia');return false;" />
<area shape="poly" coords="375,125,370,119,378,114,383,114,382,111,363,98,348,91,340,92,326,85,321,79,313,82,309,110,324,122,328,116,331,124,340,123,349,114,360,119,363,126,370,130" href="#" onclick="onCenter('madrid','La Rioja');return false;" />
<area shape="poly" coords="374,121,378,126,387,127,390,131,398,127,402,121,397,112,406,88,413,80,426,72,431,59,401,48,398,54,393,50,400,38,383,35,362,59,358,60,346,89,386,111,378,118" href="#" onclick="onCenter('madrid','Navarra');return false;" />
<area shape="poly" coords="428,123,432,118,432,100,427,105,428,74,435,61,440,70,453,68,467,76,502,77,504,116,498,129,485,147,487,162,469,165,461,151" href="#" onclick="onCenter('madrid','Huesca');return false;" />
<area shape="poly" coords="404,186,417,180,422,185,437,178,442,167,470,185,479,185,488,174,484,165,467,166,426,125,429,117,431,105,425,106,426,74,412,84,399,112,405,121,391,133,375,127,379,143,371,151,365,152,369,164,361,162,358,178,373,184,392,194" href="#" onclick="onCenter('madrid','Zaragoza');return false;" />
<area shape="poly" coords="412,253,401,247,388,239,380,228,386,217,392,213,389,195,419,181,426,184,443,178,446,168,468,185,481,186,483,191,480,208,453,214,449,234,424,265" href="#" onclick="onCenter('madrid','Teruel');return false;" />
<area shape="poly" coords="306,171,288,150,321,125,328,119,330,126,340,125,349,117,358,120,360,127,372,132,376,128,377,142,364,151,368,162,361,161,358,179,367,183,365,187,359,185,344,187,333,175,322,169" href="#" onclick="onCenter('madrid','Soria');return false;" />
<area shape="poly" coords="313,247,293,211,295,192,290,183,313,173,342,190,356,188,366,189,367,185,385,197,388,216,378,230,367,217,347,218,327,231,325,245" href="#" onclick="onCenter('madrid','Guadalajara');return false;" />
<area shape="poly" 					onclick="onCenter('madrid',1);return false;" coords="271,263,287,251,252,240,239,244,256,219,271,205,275,195,289,182,294,191,293,212,313,247,310,255,292,256,275,265" href="#" alt="Madrid" />
<area shape="poly" coords="487,166,487,146,504,116,501,67,541,75,543,93,558,91,562,103,556,105,545,138,540,136,542,149,500,169" href="#" onclick="onCenter('bcn','Lleida');return false;" />
<area shape="poly" coords="638,104,617,91,601,100,576,94,570,100,560,92,563,102,569,104,569,114,592,122,587,136,613,145,635,130,623,109" href="#" onclick="onCenter('bcn','Girona');return false;" />
<area shape="poly" 					onclick="onCenter('bcn',1);return false;" coords="549,179,573,172,581,162,610,147,586,137,588,123,568,117,568,105,556,108,549,138,542,140,540,154" href="#" alt="Barcelona" />
<area shape="poly" coords="496,222,515,211,503,206,525,186,549,180,536,153,498,171,488,169,491,175,483,185,487,195,482,212,495,222" href="#" onclick="onCenter('bcn','Tarragona');return false;" />
<area shape="poly" coords="456,276,494,224,480,210,455,219,448,241,426,266" href="#" onclick="onCenter('bcn','Castellón');return false;" />
<area shape="poly" coords="469,331,452,295,456,279,409,261,403,279,397,282,391,298,413,302,407,320,415,327,424,327,422,337,440,342,457,330" href="#" onclick="onCenter('bcn','Valencia');return false;" />
<area shape="poly" coords="435,398" href="#" /><area shape="poly" coords="423,388,429,398,437,377,456,357,480,339,458,331,439,344,423,340,413,363,418,381" href="#" onclick="onCenter('bcn','Alicante');return false;" />
<area shape="poly" coords="317,293,322,280,307,256,315,247,325,246,328,232,345,220,366,218,389,243,404,258,404,264,399,278,389,298,365,303,362,299,357,307,334,302" href="#" onclick="onCenter('madrid','Cuenca');return false;" />
<area shape="poly" coords="388,426,375,422,369,409,370,397,348,386,367,369,382,364,389,367,393,352,399,341,409,339,418,346,411,362,417,380,430,400,426,406,436,411,412,415,398,417" href="#" onclick="onCenter('madrid','Murcia');return false;" />
<area shape="poly" coords="101,305,114,296,124,298,128,309,150,312,155,318,167,317,183,316,190,311,202,303,218,298,208,285,207,272,200,273,196,253,195,243,188,246,175,238,168,241,153,228,134,242,117,242,114,249,122,257,113,280,88,280" href="#" onclick="onCenter('madrid','Cáceres');return false;" />
<area shape="poly" coords="120,375,110,376,100,353,103,342,118,319,109,316,101,308,115,299,125,300,129,311,150,314,158,321,186,318,202,306,221,298,233,294,230,306,234,313,223,315,217,342,200,348,184,362,191,381,181,382,179,375,157,389" href="#" onclick="onCenter('madrid','Badajoz');return false;" />
<area shape="poly" coords="272,306,304,291,317,292,323,279,303,256,274,265,269,264,284,252,254,242,230,247,230,240,222,242,217,252,210,251,197,253,200,271,208,268,211,284,220,297,230,293,243,286,255,290,264,285,262,298" href="#" onclick="onCenter('madrid','Toledo');return false;" />
<area shape="poly" coords="251,364,327,353,336,342,325,325,332,303,306,292,271,308,260,299,260,290,254,293,244,290,235,294,231,305,239,314,225,316,221,342" href="#" onclick="onCenter('madrid','Ciudad Real');return false;" />
<area shape="poly" coords="338,382,345,372,336,354,328,354,340,342,326,324,334,305,356,308,362,302,367,304,389,300,409,304,407,318,411,328,423,329,418,344,408,338,398,340,391,349,388,364,379,362,358,376,347,385" href="#" onclick="onCenter('madrid','Albacete');return false;" />
<area shape="poly" coords="146,464" href="#" /><area shape="poly" coords="149,462,147,422,139,409,149,401,157,401,158,392,117,376,114,386,102,389,89,415,95,441,117,438" href="#" onclick="onCenter('madrid','Huelva');return false;" />
<area shape="poly" coords="150,458,168,462,178,456,186,456,206,452,223,440,233,433,214,405,195,411,196,405,197,398,191,383,179,383,178,379,159,391,161,403,150,403,143,410,150,422" href="#" onclick="onCenter('madrid','Sevilla');return false;" />
<area shape="poly" coords="237,436,262,423,249,393,255,369,219,344,200,349,187,361,197,398,196,409,215,403" href="#" onclick="onCenter('madrid','Córdoba');return false;" />
<area shape="poly" coords="269,424,277,417,290,409,295,412,307,406,314,409,319,404,330,379,339,381,344,372,336,357,254,365,258,370,252,393,264,421" href="#" onclick="onCenter('madrid','Jaén');return false;" />
<area shape="poly" coords="188,513,202,496,187,480,197,473,199,461,207,463,207,452,176,458,166,464,149,461,144,465,142,473,151,477,150,483,163,503,170,502,178,509" href="#" onclick="onCenter('madrid','Cádiz');return false;" />
<area shape="poly" coords="210,489,224,481,229,484,248,470,279,466,272,460,245,439,249,432,237,438,232,435,208,451,208,465,201,464,197,475,192,481,205,493" href="#" onclick="onCenter('madrid','Málaga');return false;" />
<area shape="poly" coords="280,468,297,470,310,466,324,430,332,435,334,419,346,412,348,388,331,381,316,411,304,409,296,414,289,412,272,424,263,424,253,431,248,439" href="#" onclick="onCenter('madrid','Granada');return false;" />
<area shape="poly" coords="322,469,333,471,345,462,352,463,358,469,372,454,386,428,374,424,370,412,368,398,348,389,349,412,335,420,334,437,326,433,312,466" href="#" onclick="onCenter('madrid','Almeria');return false;" />
<area shape="poly" coords="151,535,186,517,256,534,150,534" href="#" onclick="onCenter('madrid','Ceuta');return false;" />
<area shape="poly" coords="322,534,334,526,351,534" href="#" onclick="onCenter('madrid','Melilla');return false;" />
</map>
				
        
        
	<div class="situation">
    	
		
        <ul>
        	<li id="lugo">
        	<h3>Centros de prevención</h3>
        	<span>
        		<strong>NORPREVENCIÓN LUGO</strong>
        	</span>
        	<div class="address">
        	<span>
        	C./Catasol 11-13 Bajo 27002 - Lugo
        	</span>
        	<span>
        	Tel. <strong>902 020 880</strong> - 982 28 41 62&nbsp;<br> Fax 982 28 45 85
        	</span>
        	</div>


        	<div class="important">
        	<span>
			Gerencia:<br> <?php echo safe_mailto('gerencia@norprevencion.com','gerencia@norprevencion.com',array('class' => 'external-link')); ?>
        	</span>
        	<span>
			Dirección financiera: <?php echo safe_mailto('finanzas@norprevencion.com','finanzas@norprevencion.com',array('class' => 'external-link')); ?>
        	</span>
        	<span>
			Dirección Administrativa: <?php echo safe_mailto('administracion@norprevencion.com','administracion@norprevencion.com',array('class' => 'external-link')); ?>
        	</span>
        	<span>
        	Dirección Técnica Nacional: <?php echo safe_mailto('dirtec@norprevencion.com','dirtec@norprevencion.com',array('class' => 'external-link')); ?>
        	</span>
        	<span>
        	Dirección Médica: <?php echo safe_mailto('dirmed@norprevencion.com','dirmed@norprevencion.com',array('class' => 'external-link')); ?>
        	</span>
        	</div>

        	<span>
        	Dirección Técnica: <?php echo safe_mailto('dirlugo@norprevencion.com','dirlugo@norprevencion.com',array('class' => 'external-link')); ?>
        	</span>
        	<span>
        	Dpto. VS: <?php echo safe_mailto('vslugo@norprevencion.com','vslugo@norprevencion.com',array('class' => 'external-link')); ?>

        	</span>
        	<span>
        	Dpto. Comercial: <?php echo safe_mailto('lugo@norprevencion.com','lugo@norprevencion.com',array('class' => 'external-link')); ?>
        	</span>

			<div class="medicalcenter">
			<h3>Centros médicos</h3>
        	<span>
        	
			Norprevención Lugo /
			Norprevención Viveiro
			</span>
			</div>
        	</li>

        	<li id="coru"><span>
        	<h3>Centros de prevención</h3>
        	<strong>NORPREVENCIÓN A CORUÑA</strong>
        	</span>
        	<div class="address">
        	<span>
        	C/ Orillamar 48-50, bajo 15001 A Coruña</span>
        	<span>
 Tel. 981 20 05 00 - Fax 981 20 84 78
        	</span>
        	</div>
        	<span>
Dirección Técnica: <?php echo safe_mailto('teccorun@norprevencion.com','teccorun@norprevencion.com',array('class' => 'external-link')); ?>
        	</span>
        	<span>Dpto. VS: <?php echo safe_mailto('vscoruna@norprevencion.com','vscoruna@norprevencion.com',array('class' => 'external-link')); ?>
        	</span>
        	<span> Dpto. Comercial: <?php echo safe_mailto('coruna@norprevencion.com','coruna@norprevencion.com',array('class' => 'external-link')); ?>
        	</span> 
        	
        	<br><br><br>
        	<span><strong>NORPREVENCIÓN SANTIAGO DE COMPOSTELA (Centro)</strong>

        	</span>
        	<div class="address">
        	<span>
        	C/ Rosalía de Castro,44 bajo 15702 Santiago de Compostela</span>
        	<span> Tel. 981 55 31 18 - Fax 981 55 31 17
        	</span>
        	</div>
        	<span>
            Dpto. Comercial: <?php echo safe_mailto('santiago@norprevencion','santiago@norprevencion.com',array('class' => 'external-link')); ?>
        	</span>
        	<br><br><br>
        	<span> <strong>NORPREVENCIÓN SANTIAGO DE COMPOSTELA (Polígono de Tambre)</strong>
        	</span>
        	<div class="address">
        	<span>Vía Galileo, nº 1, 2ª planta, 15890 Santiago de Compostela</span>
        	<span>  Tel. 981 93 77 65
        	</span>
        	</div>
        	<span>
 			Dpto. Técnico: <?php echo safe_mailto('spose@norprevencion','spose@norprevencion.com',array('class' => 'external-link')); ?>
        	</span>
        	<span> <?php echo safe_mailto('mmourelle@norprevencion','mmourelle@norprevencion.com',array('class' => 'external-link')); ?>
        	</span>
        	<div class="medicalcenter">
        	<h3>Centros médicos</h3>
        	<span>
        
			Norprevención A Coruña /
			Norprevención Santiago de Compostela
			</span>
        	</div>
        	</li>

<!--
<li  id="santiago">
        	<span><strong>NORPREVENCIÓN SANTIAGO DE COMPOSTELA (Centro)</strong>

        	</span>
        	<div class="address">
        	<span>
        	C/ Rosalía de Castro,44 bajo 15702 Santiago de Compostela</span>
        	<span> Tel. 981 55 31 18 - Fax 981 55 31 17
        	</span>
        	</div>
        	<span>
            Dpto. Comercial: <?php echo safe_mailto('santiago@norprevencion','santiago@norprevencion.com',array('class' => 'external-link')); ?>
        	</span>
        	<br><br>
        	<span> <strong>NORPREVENCIÓN SANTIAGO DE COMPOSTELA (Polígono de Tambre)</strong>
        	</span>
        	<div class="address">
        	<span>Vía Galileo, nº 1, 2ª planta, 15890 Santiago de Compostela</span>
        	<span>  Tel. 981 93 77 65
        	</span>
        	</div>
        	<span>
 			Dpto. Técnico: <?php echo safe_mailto('spose@norprevencion','spose@norprevencion.com',array('class' => 'external-link')); ?>
        	</span>
        	<span> <?php echo safe_mailto('mmourelle@norprevencion','mmourelle@norprevencion.com',array('class' => 'external-link')); ?>
        	</span>
 </li>
 -->

<li id="ourense">
<h3>Centros de prevención</h3>
<span><strong>NORPREVENCIÓN OURENSE</strong>
</span>
<div class="address">
<span>C/ Emilia Pardo Bazán 32 Bajo 32004 Ourense</span>
<span> Tel. 988 39 19 63 - Fax 988 39 19 64</span>
</div>
<span>
  Dirección técnica: <?php echo safe_mailto('tecourense@norprevencion','tecourense@norprevencion.com',array('class' => 'external-link')); ?></span>
<span> Dpto. VS: <?php echo safe_mailto('vsourense@norprevencion','vsourense@norprevencion.com',array('class' => 'external-link')); ?></span>
<span> Dpto. Comercial: <?php echo safe_mailto('ourense@norprevencion','ourense@norprevencion.com',array('class' => 'external-link')); ?> </span>
 <div class="medicalcenter">
 
 <h3>Centros médicos</h3>
        	<span>
        
			Norprevención Ourense
			</span>
        	</div> 
 </li>


<li id="ponte">
<h3>Centros de prevención</h3>
<span><strong>NORPREVENCIÓN VIGO</strong></span>
<div class="address">
<span> C/ Padre Ramón María Aller 6 Bajo 36201 Vigo</span>
<span> Tel. 986 22 54 25 Fax 986 22 81 24</span>
</div>
<span>
Dirección técnica: <?php echo safe_mailto('tecvigo@norprevencion','tecvigo@norprevencion.com',array('class' => 'external-link')); ?></span>
<span>
Dpto. VS: <?php echo safe_mailto('vsvigo@norprevencion','vsvigo@norprevencion.com',array('class' => 'external-link')); ?></span>
<span> Dpto. Comercial: <?php echo safe_mailto('vigo@norprevencion','vigo@norprevencion.com',array('class' => 'external-link')); ?></span>

<br><br><br>
<span>
<strong>NORPREVENCIÓN A ESTRADA </strong></span>
<div class="address">
<span>C/ Serafín Pazo nº 28, 2º A. 36680 A Estrada</span>
<span> Tfno: 986 59 02 28 - Fax: 986 59 02 29</span>
</div>
<span>

Dirección técnica A Estrada: <?php echo safe_mailto('tecvigo@norprevencion','tecvigo@norprevencion.com',array('class' => 'external-link')); ?></span>
<span>
Dpto. VS: <?php echo safe_mailto('vsvigo@norprevencion','vsvigo@norprevencion.com',array('class' => 'external-link')); ?></span>
<span> Dpto. Comercial: <?php echo safe_mailto('vigo@norprevencion','vigo@norprevencion.com',array('class' => 'external-link')); ?></span>
<div class="medicalcenter">
<h3>Centros médicos</h3>

			<span>
        	
			Norsalud Pontevedra /
			Norprevención Pontevedra
			</span>
</div>
</li>
<!--
<li id="estrada">
<span>
<strong>NORPREVENCIÓN A ESTRADA </strong></span>
<div class="address">
<span>C/ Serafín Pazo nº 28, 2º A. 36680 A Estrada</span>
<span> Tfno: 986 59 02 28 - Fax: 986 59 02 29</span>
</div>
<span>

Dirección técnica A Estrada: <?php echo safe_mailto('tecvigo@norprevencion','tecvigo@norprevencion.com',array('class' => 'external-link')); ?></span>
<span>
Dpto. VS: <?php echo safe_mailto('vsvigo@norprevencion','vsvigo@norprevencion.com',array('class' => 'external-link')); ?></span>
<span> Dpto. Comercial: <?php echo safe_mailto('vigo@norprevencion','vigo@norprevencion.com',array('class' => 'external-link')); ?></span>

</li>
-->

<li id="asturias">
<h3>Centros de prevención</h3>
<span>
<strong>NORPREVENCIÓN ASTURIAS</strong></span>
<div class="address">
<span> Parque Tecnológico de Asturias, Edificio Centroelena Fase II, 45-46 - Bajo 33428 - Llanera-Asturias</span>
<span> Tel. 985 73 36 66 Fax 985 73 34 44 </span>
</div>
<span> Dpto. Comercial: <?php echo safe_mailto('asturias@norprevencion','asturias@norprevencion.com',array('class' => 'external-link')); ?>  </span>
<span>Dirección Técnica: <?php echo safe_mailto('tecasturias@norprevencion','tecasturias@norprevencion.com',array('class' => 'external-link')); ?></span>

<div class="medicalcenter">
        	<h3>Centros médicos</h3>
			 	<span>
			 Llanera /
			Norprevención Asturias
			</span>
</div>



</li>

<li  id="leon">
<h3>Centros de prevención</h3>
<span>
<strong>NORPREVENCIÓN LEÓN</strong> </span>
<div class="address">
<span>C/ Luis Carmona, 3 24007 León</span>
<span>
 Tel. 987 84 00 53&nbsp; Fax 987 84 06 12</span>
 </div>
<span>  Dirección técnica: <?php echo safe_mailto('tecleon@norprevencion','tecleon@norprevencion.com',array('class' => 'external-link')); ?></span>
<span> Dpto. Comercial: <?php echo safe_mailto('leon@norprevencion','leon@norprevencion.com',array('class' => 'external-link')); ?></span>

</li>

<li  id="madrid">
<h3>Centros de prevención</h3>
<span>
<strong>NORPREVENCIÓN MADRID</strong></span>
<div class="address">
<span> C/ Rufino González 40 1º izda. 28037 Madrid</span>
<span>
 Tel. 91 713 06 65 Fax 91 713 06 69  </span>
 </div>
<span>Dirección técnica: <?php echo safe_mailto('tecmadrid@norprevencion','tecmadrid@norprevencion.com',array('class' => 'external-link')); ?></span>
<span> Dpto. VS: <?php echo safe_mailto('vsmadrid@norprevencion','vsmadrid@norprevencion.com',array('class' => 'external-link')); ?> </span>
<span>Dpto. Comercial: <?php echo safe_mailto('madrid@norprevencion','madrid@norprevencion.com',array('class' => 'external-link')); ?></span>
<div class="medicalcenter">
<h3>Centros médicos</h3>
<span>
        	
			Norprevención Madrid
			</span>

</div>
</li>

<li id="bcn">
<h3>Centros de prevención</h3>
<span><strong>NORPREVENCIÓN BARCELONA</strong></span>
<div class="address">
<span>
 C/ Doctor Pi i Molist 78-80, 1º. 08016. Barcelona. </span>
<span> Tel. 93 408 35 17 - Fax: 93 243 20 51 </span>
</div>
<span> Dirección Territorial:<?php echo safe_mailto('dirbarcelona@norprevencion',' dirbarcelona@norprevencion.com',array('class' => 'external-link')); ?> </span>
<span>Dpto. Técnico: <?php echo safe_mailto('dirbarcelona@norprevencion','dirbarcelona@norprevencion.com',array('class' => 'external-link')); ?> </span>
<span>Dpto. Comercial: <?php echo safe_mailto('barcelona@norprevencion','barcelona@norprevencion.com',array('class' => 'external-link')); ?></span>
</li>
</ul>

<p class="moreinfo">Para más información, póngase en contacto con 
nosotros a través del e-mail: <?php echo safe_mailto('info@norprevencion.com','info@norprevencion.com',array('class' => 'external-link')); ?> o el número de teléfono <b>902 020 
880</b></p>

</div>
    </section>


	<!--
    <section class="medical-situation">
    <header>
		<h3>Centros de prevención</h3>

		<ul>
			<li><strong>Centros de prevención DE A CORUÑA</strong>
			<strong>A Coruña</strong>.
			Norprevención A Coruña <strong>Santiago</strong>.
			Norprevención Santiago de Compostela
			</li>

			<li><strong>Centros de prevención DE LUGO Lugo</strong>.
			Norprevención Lugo<strong>Viveiro</strong>.
			Norprevención Viveiro
			</li>

			<li><strong>Centros de prevención DE OURENSE Ourense</strong>.
			Norprevención Ourense</li>

			<li><strong>Centros de prevención DE PONTEVEDRA Vigo</strong>.
			Norprevención Pontevedra <strong>Pontevedra</strong>.
			Norsalud Pontevedra</li>

			<li><strong>CENTRO MÉDICO DE ASTURIAS Llanera</strong>.
			Norprevención Asturias</li>

			<li><strong>Centros de prevención DE MADRID</strong>
			<strong>Madrid</strong>.
			Norprevención Madrid</li>

			<li><strong>UNIDADES MÓVILES</strong></li>
			</ul>
    </header>
    </section>
    -->

</div>

<script>

var str_centers = "Centros de prevención";
var str_centers_near = "más cercanos a";

function onCenter(center , principal){
	var text = str_centers;
	$('#'+center+' h3:first').text(str_centers);
	if(principal!=1){
		var text = $('#'+center+' h3:first').text();
		$('#'+center+' h3:first').text(text + ' '+str_centers_near+' '+ principal);
		$('.medicalcenter').hide();
	}else{
		$('.medicalcenter').show();
	}
    hideCenters();
    $('#'+center).show();
}

function hideCenters(){
    $('.situation ul li').hide();
}

window.onload = function (){
    hideCenters();
    onCenter('lugo' , 1);
}

</script>

<?php $this->load->view('common/base_end'); ?>