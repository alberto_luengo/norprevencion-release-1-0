<?php $this->load->view('common/base_begin'); ?>

<div class="estimation">
	<div>

    <section>
        <header>
            <h2>Solicita presupuesto</h2>
        </header>

            <?php if(!$msn){ ?>

            <form id="form" action="/<?php echo LANG;?>/presupuestos" method="post">
                <fieldset>
                    <legend>Solicitud de presupuesto sin compromiso</legend>
                    	<div>
                            <label for="name">Razón Social / Nombre de la empresa</label>
                            <input name="name" id="name" value="" type="text">
                     	</div>


                        <div id="centers">
                            <?php $this->load->view('sections/presupuestos_centers_form.php');?>
                        </div>

						<div  class="add" >
                        <input type="button" onclick="addCenter();return false;" value="Añadir centro de trabajo">
						</div>

						<div>
                            <label for="activity">E-mail</label>
                            <input name="mail" id="mail" value="" type="text">
                        </div>

                    	<div>
                        <label for="comment">Comentarios</label>
                        <textarea name="comment" id="comment" rows="5" cols="20"></textarea>
                    	</div>
                    <div>
                        <input value="ENVIAR" type="submit">
                    </div>
                </fieldset>

            </form>
            <script type="text/javascript">

                function addCenter(){
                    $.ajax({url: "/<?php echo LANG;?>/presupuestos/addcenter",cache: false, success: function(data){
                        $('#centers').append(data);
                    }});
                }
                function delCenter(id){
                    $('#center_'+id).remove();
                }

            </script>
            <?php }else{ ?>

                <p class="accepted">
                    <?php echo $msn; ?>
                </p>

            <?php } ?>


    </section>
	<aside>
		<h3>¿Quieres solicitar un presupuesto?</h3>
			<p>
			Si deseas solicitar un presupuesto puedes ponerte 
en contacto con nosotros mediante este formulario,  mandando un e-mail  a <?php echo safe_mailto('comercial@norprevencion.com','comercial@norprevencion.com',array('class' => 'external-link')); ?> o llamando al  teléfono <b>902 020 880</b>
			
		
			</p>
			<p>Nuestros comerciales contactarán contigo para informarte.
			</p>
		</aside>



	</div>

</div>

<?php $this->load->view('common/base_end'); ?>
