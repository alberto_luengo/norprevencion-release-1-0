
<?php $id = uniqid(); ?>
<div id="center_<?php echo $id;?>">

    <div>
        <label for="activity_<?php echo $id;?>">Actividad</label>
        <input name="activity_<?php echo $id;?>" id="activity_<?php echo $id;?>" value="" type="text">
    </div>

    <div class="doble">
        <label for="centername_<?php echo $id;?>">Nombre centro</label>
        <input name="centername_<?php echo $id;?>" id="centername_<?php echo $id;?>" value="" type="text">
    </div>

    <div class="doble">
        <label for="province_<?php echo $id;?>">Provincia</label>
        <input name="province_<?php echo $id;?>" id="province_<?php echo $id;?>" value="" type="text">
    </div>

    <div class="doble">
        <label for="population_<?php echo $id;?>">Población</label>
        <input name="population_<?php echo $id;?>" id="population_<?php echo $id;?>" value="" type="text">
    </div>

    <div class="doble">
        <label for="home_<?php echo $id;?>">Domicilio</label>
        <input name="home_<?php echo $id;?>" id="home_<?php echo $id;?>" value="" type="text">
    </div>

    <div class="doble">
        <label for="phone_<?php echo $id;?>">Teléfono</label>
        <input name="phone_<?php echo $id;?>" id="phone_<?php echo $id;?>" value="" type="text">
    </div>

    <div class="doble">
        <label for="workers_<?php echo $id;?>">Número de trabajadores</label>
        <input name="workers_<?php echo $id;?>" id="workers_<?php echo $id;?>" value="" type="text">
    </div>

    <div class="choose">
      <label>Centro principal</label>
      	<p>
        	<input value="1" name="center_<?php echo $id;?>" type="radio">
        	SI
    	</p>
    	<p>
        	<input value="0" name="center_<?php echo $id;?>" type="radio">
        	NO
    	</p>
    </div>

<?php if(isset($DELETE_FORM)){ ?>
<div  class="del" >
 <input type="button" onclick="delCenter('<?php echo $id;?>');return false;" value="Eliminar este centro de trabajo">
 </div>
<?php } ?>

</div>