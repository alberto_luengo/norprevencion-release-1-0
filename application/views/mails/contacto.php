<html>
    <head>
        <title>Contacto</title>
        <meta charset="utf-8">
        <style type="text/css">
            body{margin: 20px 20px 20px 20px;}
            p{ text-align:justify;}
            h1{color:#3e5193;}
            h2{color:#999999;}
            img{margin-top:20px;}
            hr { border: 1px solid #cccccc}
            a{color: #000;}
            a:hover{ text-decoration: none; color: #000;}
        </style>
    </head>
    <body>

        <h1>
           Formulario de contacto via web
        </h1>

        <hr />

        <h2>
            Nombre: <?php echo trim($datos['name']);;?>
        </h2>

        <h2>
            Email: <?php echo trim($datos['mail']);?>
        </h2>

        <h2>
            Motivo: <?php echo trim($datos['reason']);?>
        </h2>

        <hr />

        <p>
            <?php echo nl2br(trim($datos['comment']));?>
        </p>

    </body>
</html>

</html>