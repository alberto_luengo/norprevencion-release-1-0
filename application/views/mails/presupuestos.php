<html>
    <head>
        <title>Presupuestos</title>
        <meta charset="utf-8">
        <style type="text/css">
            body{margin: 20px 20px 20px 20px;}
            p{ text-align:justify;}
            h1{color:#3e5193;}
            h2{color:#999999;}
            img{margin-top:20px;}
            hr { border: 1px solid #cccccc}
            a{color: #000;}
            a:hover{ text-decoration: none; color: #000;}
        </style>
    </head>
    <body>

        <h1>
           Formulario de presupuesto via web
        </h1>

        <hr />

        <h2>
            Razón Social / Nombre de la empresa: <?php echo $name;?>
        </h2>

        <h2>
           E-Mail <?php echo $mail;?>
        </h2>

        <h2>
            Centros:
        </h2>

        <?php foreach ($centers as $key => $value) {?>
            <h3>
               Nombre del centro: <?php echo $centers[$key]['centername']; ?>
            </h3>
            <h3>
              Actividad: <?php echo $centers[$key]['activity']; ?>
            </h3>
            <h3>
               Provincia: <?php echo $centers[$key]['province']; ?>
            </h3>
            <h3>
               Población: <?php echo $centers[$key]['population']; ?>
            </h3>
            <h3>
               Domicilio: <?php echo $centers[$key]['home']; ?>
            </h3>
             <h3>
               Número de trabajadores: <?php echo $centers[$key]['workers']; ?>
            </h3>
            <h3>
               Centro principal: <?php if($centers[$key]['center']==1){ echo 'SI';}else{echo 'NO';} ?>
            </h3>
            <hr />
        <?php } ?>

        <p>
            <?php echo nl2br($comment);?>
        </p>

    </body>
</html>

</html>