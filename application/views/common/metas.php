
<!-- Desing by: Sond3 -->
<!-- Sond3 web: http://www.sond3.com -->
<!-- Sond3 contact: info@sond3.com -->

<!-- METAS  -->
<meta charset="utf-8">

<meta name="viewport" content="width=device-width">

<title><?php echo $meta_title;?></title>

<meta name="description" content="<?php echo $meta_description;?>">
<meta name="keywords" content="<?php echo $meta_keywords;?>">
<meta name="application-name" content="norprevencion.com">
<meta name="revisit-after" content="7 days">
<meta name="robots" content="index,follow">

<meta property="og:site_name" content="norprevencion.com">
<meta property="og:url" content="norprevencion.com>">
<meta property="og:title" content="<?php echo $meta_title;?>">
<meta property="og:description" content="<?php echo $meta_description;?>">

<!-- /METAS  -->
