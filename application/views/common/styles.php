
<!-- FAVICON -->
<link rel="shortcut icon" href="/favicon.ico">
<!-- /FAVICON -->

<!-- STYLES  -->
<link href="/<?php echo LANG?>/css" rel="stylesheet">
<link href='http://fonts.googleapis.com/css?family=Archivo+Narrow:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
<!-- /STYLES  -->

<!--[if lt IE 9]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

