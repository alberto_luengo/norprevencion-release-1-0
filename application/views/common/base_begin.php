<?php $this->view('common/doctype'); ?>
<head>
    <?php $this->view('common/metas'); ?>
    <?php $this->view('common/styles'); ?>
</head>
<body>
    <!-- PAGE-WRAPPER -->
    <div id="page">

    	<?php $this->view('common/header'); ?>

    	<?php
    	if($PROMO){
    		$this->view('common/promo');
    	}
    	?>

    	<!-- CONTENT -->
		<div id="content">
