<!-- HEADER -->
    <div id="header">
		<header>
			<h1 class="logo imageback"><a href="/<?php echo LANG;?>">Norprevención</a></h1>
			<p class="description">Prevención<br> de riesgos laborales</p>

			<div class="options">

				<?php $this->load->view('common/menu_lang');?>
				<?php $this->load->view('common/menu_access');?>

			</div>

		</header>

		<?php $this->load->view('common/menu');?>

	</div>
<!-- /HEADER -->