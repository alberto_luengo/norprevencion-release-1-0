
<nav id="navsite">
    <h2>Site navigation</h2>
    <ul class="menu-site">
        <?php $active =''; if($menu=='home'){ $active = 'class="active"';}?>
        <li><a <?php echo $active;?> href="/<?php echo LANG;?>">Norprevención</a></li>
        <li>
            <?php $active =''; if($menu=='servicios'){ $active = 'class="active"';}?>
            <a  <?php echo $active;?> href="/<?php echo LANG;?>/servicios">Servicios</a>

            <?php if($SHOW_SUBMENU){ ?>
            <ul class="submenu">
            	<!--
                <?php $active =''; if($submenu=='ajenos'){ $active = 'class="active"';}?>
                <li><a <?php echo $active;?> href="/<?php echo LANG;?>/servicios/ajenos-de-prevencion">Ajenos de prevención</a></li>
                -->
                <?php $active =''; if($submenu=='asistencia'){ $active = 'class="active"';}?>
                <li><a <?php echo $active;?> href="/<?php echo LANG;?>/servicios/asistencia-tecnica">Asistencia Técnica</a></li>
                <?php $active =''; if($submenu=='consultoria'){ $active = 'class="active"';}?>
                <li><a <?php echo $active;?> href="/<?php echo LANG;?>/servicios/consultoria-servicios-especiales">Consultoría y Servicios Especiales</a></li>
                <?php $active =''; if($submenu=='vigilancia'){ $active = 'class="active"';}?>
                <li><a <?php echo $active;?> href="/<?php echo LANG;?>/servicios/vigilancia-salud">Vigilancia de la Salud</a></li>
                <?php $active =''; if($submenu=='formacion'){ $active = 'class="active"';}?>
                <li><a <?php echo $active;?> href="/<?php echo LANG;?>/servicios/formacion">Formación</a></li>
            </ul>
            <?php } ?>

        </li>
         <?php $active =''; if($menu=='centros'){ $active = 'class="active"';}?>
        <li><a <?php echo $active;?> href="/<?php echo LANG;?>/centros">Red de centros</a>
        </li>
         <?php $active =''; if($menu=='novedades'){ $active = 'class="active"';}?>
        <li><a <?php echo $active;?> href="/<?php echo LANG;?>/novedades">Actualidad</a></li>
        <!--<li><a href="#">Enlaces?</a></li>-->
         <?php $active =''; if($menu=='contacto'){ $active = 'class="active"';}?>
        <li><a <?php echo $active;?> href="/<?php echo LANG;?>/contacto">Contacto</a></li>
         <?php $active =''; if($menu=='empleo'){ $active = 'class="active"';}?>
        <li class="work"><a <?php echo $active;?> href="/<?php echo LANG;?>/empleo">Trabaja con nosotros</a></li>
         <?php $active =''; if($menu=='presupuestos'){ $active = 'class="active"';}?>
        <li class="budget"><a <?php echo $active;?> href="/<?php echo LANG;?>/presupuestos"><span>SOLICITA</span> <span>PRESUPUESTO</span></a></li>
    </ul>
</nav>
