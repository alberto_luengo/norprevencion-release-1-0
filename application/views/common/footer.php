<!-- FOOTER -->
 	<footer id="footer">
		<div class="main"> 
			<div>
				<h4>Información corporativa</h4>
				<ul>
					<!--
					<li><a href="/<?php echo LANG;?>">Empresa</a></li>
					-->
					<li><a href="/<?php echo LANG;?>/centros">Red de Centros</a></li>
					<!--
					<li><a href="/<?php echo LANG;?>/empleo">Empleo / Trabaja con nosotros</a></li>
					-->
					<li><a href="/<?php echo LANG;?>/servicios">Servicios / Especialidades</a></li>
				</ul>
				</div>
				<div>
				<h4>Bases legales</h4>
				<ul>
					<li><a href="/<?php echo LANG;?>/legal/politica">Política de protección de datos</a></li>
					<li><a href="/<?php echo LANG;?>/legal/contenido">Contenido y uso de la web</a></li>
					<li><a href="/<?php echo LANG;?>/legal/responsabilidades">Responsabilidades y resolución de conflictos</a></li>
				</ul>
				</div>
				<div>
				<h4>Contacto</h4>
				<ul>
					<li><a href="/<?php echo LANG;?>/contacto">Consultas y reclamaciones</a></li>
					<!--
					<li><a href="#">Colaboración</a></li>
					<li><a href="#">Incidencias</a></li>
					-->
					<li><a href="/<?php echo LANG;?>/empleo">Trabaja con nosotros</a></li>
					<li><a href="/<?php echo LANG;?>/presupuestos">Solicita presupuesto</a></li>
				</ul>
				</div>
				<div>
				<h4>Calidad</h4>
				<img src="/img/9001.png" alt="certificado de calidad 9001" width=100 height=208 />
				<img src="/img/14001.png" alt="certificado de calidad 14001" width=100 height=208 />
				<img src="/img/18001.png" alt="certificado de calidad 18001" width=100 height=208 />
				</div>
			</div>
		<p class="colofon">Copyright 2012 Norprevención - T. 902 020 880 - <a href="/<?php echo LANG;?>/centros">Más información</a></p>
	</footer>
<!-- /FOOTER -->