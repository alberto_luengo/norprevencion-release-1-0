<!-- PROMO -->
		<div id="promo">
			<div id="slides">
            		<div class="slides_container">
                		<div class="container">
                    		<img src="/img/promo/seguridad.png" alt="seguridad" width=280 height=280 />
                    		<div class="inside">
                    			<h2>
                    				Seguridad
								</h2>
								<h3>

								La prevención de los accidentes de trabajo
									y enfermedades profesionales ha constituído
									desde su fundación un objetivo básico y
									prioritario para Norprevención. </h3>
								<p><a href="/<?php echo LANG;?>/servicios/asistencia-tecnica">Más información...</a></p>
                    		</div>
               	 		</div>

               	 		<div class="container">
                    		<img src="/img/promo/medicina.png" alt="medicina" width=280 height=280 />
                    		<div class="inside">
                    			<h2>
                    				Medicina del trabajo
								</h2>
								<h3>Pensando en una mejor atención y cercanía a todos nuestros clientes disponemos de Clínicas de Medicina Laboral en cada una de nuestras delegaciones, así como una red de Clínicas concertadas distribuidas por todo el territorio nacional. </h3>
 								<p><a href="/<?php echo LANG;?>/servicios/vigilancia-salud">Más información...</a></p>
                    		</div>
               	 		</div>

               	 		<div class="container">
                    		<img src="/img/promo/formacion.png" alt="formacion consultoría" width=280 height=280 />
                    		<div class="inside">
                    			<h2>
                    				Formación
								</h2>
								<h3>La prevención de los accidentes de trabajo y de las enfermedades profesionales es 
								una prioridad para empresarios y trabajadores. Mediante nuestras acciones 
formativas conseguirá cumplir con la Ley, reducir costes y mejorar la gestión de la seguridad y salud.</h3>
 								<p><a href="/<?php echo LANG;?>/servicios/formacion">Más información...</a></p>
                    		</div>
               	 		</div>
               	 		<div class="container">
                    		<img src="/img/promo/consultoria.png" alt="formacion consultoría" width=280 height=280 />
                    		<div class="inside">
                    			<h2>
                    				Consultoría
								</h2>
								<h3>Analizamos las fortalezas y debilidades de su organización, dándole todo lo necesario para que esté al día y actualizado en materia de 
prevención.</h3>
 								<p><a href="/<?php echo LANG;?>/servicios/consultoria-servicios-especiales">Más información...</a></p>
                    		</div>
               	 		</div>


               	 		<div class="container">
                    		<img src="/img/promo/psicologia.png" alt="formacion consultoría" width=280 height=280 />
                    		<div class="inside">
                    			<h2>
                    				Ergonomía y Psicosociología aplicada
								</h2>
								<h3>Nos ocupamos de  adaptar el lugar y medios de trabajo al trabajador, poniendo los recursos necesarios para 
ello.</h3>
 								<p><a href="/<?php echo LANG;?>/servicios/ajenos-de-prevencion">Más información...</a></p>
                    		</div>
               	 		</div>

               	 		<div class="container">
                    		<img src="/img/promo/higiene.png" alt="formacion consultoría" width=280 height=280 />
                    		<div class="inside">
                    			<h2>
                    			Higiene industrial
								</h2>
								<h3>Estudiamos el medio ambiente físico, químico o biológico del trabajo para prevenir el desarrollo de enfermedades profesionales. </h3>
 								<p><a href="/<?php echo LANG;?>/servicios/vigilancia-salud">Más información...</a></p>
                    		</div>
               	 		</div>



            		</div>
        	</div>
		</div>
		<!-- /PROMO -->